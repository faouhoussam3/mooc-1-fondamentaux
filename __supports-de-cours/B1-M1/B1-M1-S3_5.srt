1
00:00:00,888 --> 00:00:02,200
Dans la vidéo précédente,

2
00:00:02,300 --> 00:00:03,500
nous avons vu comment représenter

3
00:00:03,600 --> 00:00:05,224
des nombres négatifs en binaire

4
00:00:05,324 --> 00:00:07,549
c'est-à-dire sans devoir recourir au signe -.

5
00:00:07,910 --> 00:00:09,339
Dans cette vidéo, nous allons voir

6
00:00:09,439 --> 00:00:10,664
comment représenter des nombres

7
00:00:10,764 --> 00:00:12,635
avec une partie fractionnaire

8
00:00:12,735 --> 00:00:14,578
sans devoir utiliser la virgule.

9
00:00:15,599 --> 00:00:17,191
Supposons que nous voulons représenter

10
00:00:17,291 --> 00:00:19,207
l'information sur 32 bits.

11
00:00:19,667 --> 00:00:23,663
Considérons le nombre 7,625

12
00:00:23,763 --> 00:00:27,299
qui se représente 111,101 en base 2.

13
00:00:27,619 --> 00:00:29,103
On peut l'écrire sur 32 bits

14
00:00:29,203 --> 00:00:31,492
en ajoutant des 0 à gauche et à droite.

15
00:00:31,592 --> 00:00:33,665
Observons bien que ces 0 supplémentaires

16
00:00:33,765 --> 00:00:35,470
ne modifient pas la valeur du nombre.

17
00:00:36,983 --> 00:00:39,380
On peut alors imaginer une technique simple

18
00:00:39,480 --> 00:00:40,796
pour représenter ces nombres

19
00:00:40,896 --> 00:00:43,238
qui consiste à fixer arbitrairement

20
00:00:43,338 --> 00:00:44,436
la position de la virgule.

21
00:00:44,536 --> 00:00:46,274
Par exemple, on pourrait dire que

22
00:00:46,374 --> 00:00:47,511
les 16 bits de poids fort

23
00:00:47,611 --> 00:00:48,711
représentent la partie entière,

24
00:00:48,811 --> 00:00:50,203
et que les 16 bits de poids faible

25
00:00:50,303 --> 00:00:51,812
représentent la partie fractionnaire.

26
00:00:51,912 --> 00:00:52,957
Dans notre exemple,

27
00:00:53,057 --> 00:00:55,341
on obtient donc cette représentation.

28
00:00:55,818 --> 00:00:57,984
C'est une solution simple

29
00:00:58,084 --> 00:01:00,063
mais en fait elle est trop simpliste.

30
00:01:00,163 --> 00:01:01,548
Avec cette solution par exemple,

31
00:01:01,648 --> 00:01:02,971
on ne peut pas représenter

32
00:01:03,071 --> 00:01:04,881
la valeur 2 exposant -17

33
00:01:04,981 --> 00:01:05,950
alors que cette valeur

34
00:01:06,050 --> 00:01:07,540
tient sur 18 bits en binaire.

35
00:01:07,996 --> 00:01:09,375
On voit bien que le problème

36
00:01:09,475 --> 00:01:11,656
est que la position de la virgule est fixe.

37
00:01:11,756 --> 00:01:12,571
De ce fait,

38
00:01:12,671 --> 00:01:14,734
les bits de la partie entière

39
00:01:14,834 --> 00:01:15,860
ne peuvent pas être utilisés

40
00:01:15,960 --> 00:01:18,432
pour gagner de la précision dans la partie fractionnaire

41
00:01:18,532 --> 00:01:19,236
et vice-versa.

42
00:01:19,813 --> 00:01:21,566
En pratique, la solution retenue

43
00:01:21,666 --> 00:01:24,342
est donc celle de la virgule flottante.

44
00:01:24,442 --> 00:01:26,124
Et celle-ci consiste à réserver

45
00:01:26,224 --> 00:01:28,577
une partie de la représentation à un entier

46
00:01:28,677 --> 00:01:30,454
qui indique la position de la virgule.

47
00:01:31,076 --> 00:01:34,251
On en trouve un exemple dans la norme IEEE754

48
00:01:34,351 --> 00:01:35,750
qui est une norme industrielle utilisée

49
00:01:35,850 --> 00:01:37,326
par de nombreux microprocesseurs.

50
00:01:37,471 --> 00:01:39,676
Nous allons maintenant en expliquer l'idée

51
00:01:39,776 --> 00:01:41,116
sur un exemple sur 32 bits.

52
00:01:42,047 --> 00:01:45,051
Considérons le nombre 0,0101

53
00:01:45,151 --> 00:01:47,844
à représenter selon la norme IEEE754.

54
00:01:48,225 --> 00:01:50,168
On commence par déplacer la virgule

55
00:01:50,268 --> 00:01:51,769
de façon à obtenir un nombre de la forme

56
00:01:51,869 --> 00:01:52,954
1 virgule quelque chose.

57
00:01:53,054 --> 00:01:54,763
Pour compenser ce déplacement,

58
00:01:54,863 --> 00:01:56,876
on doit multiplier cette représentation

59
00:01:56,976 --> 00:01:58,031
par une puissance de 2.

60
00:01:58,294 --> 00:02:00,013
Dans notre cas, on doit déplacer la virgule

61
00:02:00,113 --> 00:02:01,652
de deux positions vers la gauche

62
00:02:01,752 --> 00:02:04,070
ce qui revient à multiplier par 2 exposant -2.

63
00:02:04,557 --> 00:02:05,732
On voit maintenant apparaître

64
00:02:05,832 --> 00:02:08,648
les deux éléments principaux de la représentation.

65
00:02:08,748 --> 00:02:11,156
D'une part, les bits de données 01

66
00:02:11,256 --> 00:02:13,347
qui sont ceux qui apparaissent après la virgule,

67
00:02:13,447 --> 00:02:14,851
puisqu'on sait de toute manière

68
00:02:14,951 --> 00:02:16,734
qu'il y aura toujours un 1 avant la virgule.

69
00:02:16,834 --> 00:02:17,953
D'autre part,

70
00:02:18,053 --> 00:02:20,031
l'entier qui indique la position de la virgule

71
00:02:20,131 --> 00:02:21,650
c'est-à-dire l'exposant -2

72
00:02:21,750 --> 00:02:22,785
de la puissance de 2.

73
00:02:23,211 --> 00:02:25,311
Ces deux informations sont suffisantes

74
00:02:25,411 --> 00:02:28,775
pour reconstruire la valeur 0,0101.

75
00:02:29,347 --> 00:02:31,294
Il faudra y ajouter une troisième

76
00:02:31,394 --> 00:02:33,716
au cas où le nombre à représenter est négatif.

77
00:02:34,058 --> 00:02:35,063
Voyons maintenant comment

78
00:02:35,163 --> 00:02:36,429
on représente tout cela en pratique.

79
00:02:36,900 --> 00:02:39,108
Les 32 bits de la représentation sont divisés

80
00:02:39,208 --> 00:02:42,133
en un bit de signe qui vaut 1 quand le nombre est négatif,

81
00:02:42,233 --> 00:02:45,796
8 bits qui représentent l'exposant de la puissance de 2,

82
00:02:45,976 --> 00:02:49,213
et 23 bits pour les bits après la virgule.

83
00:02:50,610 --> 00:02:52,228
Reprenons notre exemple.

84
00:02:52,328 --> 00:02:53,478
On commence par remplir

85
00:02:53,578 --> 00:02:55,114
les 23 bits de données.

86
00:02:55,214 --> 00:02:56,996
On y indique 01

87
00:02:57,096 --> 00:02:59,505
suivis de 21 zéros pour avoir 23 bits.

88
00:02:59,605 --> 00:03:01,897
Attention à bien ajouter les 0 à droite

89
00:03:01,997 --> 00:03:03,824
afin de ne pas modifier la partie fractionnaire.

90
00:03:04,455 --> 00:03:06,974
Ensuite on peut mettre le bit de signe à 0.

91
00:03:07,074 --> 00:03:09,614
Finalement, on peut stocker la valeur -2

92
00:03:09,714 --> 00:03:10,994
dans les 8 bits d'exposant.

93
00:03:12,109 --> 00:03:13,073
Comme cette valeur est signée,

94
00:03:13,173 --> 00:03:15,278
il faut choisir une technique pour les nombres négatifs.

95
00:03:15,585 --> 00:03:18,248
Dans ce cas, on n'utilisera pas le complément à 2,

96
00:03:18,348 --> 00:03:20,594
mais l'excès à 127.

97
00:03:21,054 --> 00:03:23,940
Cette technique consiste à ajouter 127

98
00:03:24,040 --> 00:03:25,354
à la valeur à représenter

99
00:03:25,454 --> 00:03:27,135
pour avoir un nombre positif.

100
00:03:27,235 --> 00:03:29,612
On représente ensuite ce nombre positif

101
00:03:29,712 --> 00:03:30,512
en base 2.

102
00:03:30,612 --> 00:03:31,476
Dans notre exemple,

103
00:03:31,576 --> 00:03:33,937
-2 plus 127 vaut 125

104
00:03:34,037 --> 00:03:37,151
ce qui se représente 0111 1101

105
00:03:37,251 --> 00:03:37,954
en binaire.

106
00:03:39,000 --> 00:03:42,438
Cet exemple donne une idée de la norme IEEE754

107
00:03:42,538 --> 00:03:43,853
qui est en fait beaucoup plus étendue.

108
00:03:43,953 --> 00:03:46,138
En particulier, on peut noter qu'il faut trouver

109
00:03:46,238 --> 00:03:48,464
une représentation spéciale pour le 0,

110
00:03:48,564 --> 00:03:50,998
ou encore qu'il existe des versions plus précises

111
00:03:51,098 --> 00:03:52,098
sur 64 ou 128 bits.

