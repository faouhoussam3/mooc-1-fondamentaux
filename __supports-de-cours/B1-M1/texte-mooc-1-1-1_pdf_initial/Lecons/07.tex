\documentclass[a4paper]{scrartcl}
\usepackage{mathptmx}
\usepackage[scaled=.90]{helvet}
\usepackage{courier}

\usepackage[french]{babel}
\usepackage[T1]{fontenc}
%\usepackage{a4wide}
\usepackage[pdftex]{graphicx}
\usepackage{url}
\usepackage{../../foncordi}
%\usepackage{hyperref}
%\usepackage{times}
\usepackage{rotating}
\usepackage{amssymb,amsmath}
%\newcommand{\xor}{\ensuremath{\operatorname{\mathrm{XOR}}}}
\newcommand{\desc}{\ensuremath{\textrm{desc}}}
\newcommand{\pres}{\ensuremath{\textrm{pr�sence}}}

\title{INFO-F-102 -- Fonctionnement des Ordinateurs\\ Le�on 14
  -- Interruptions et \textit{traps}} \author{Gilles Geeraerts} \date{Ann�e
  acad�mique 2016--2017}

\begin{document}
\maketitle

On appelle <<~flux de contr�le~>> l'ordre dans lequel les instructions
sont ex�cut�es. Normalement, la boucle d'interpr�tation du processeur
fait en sorte que les instructions sont ex�cut�es dans l'ordre o�
elles apparaissent en m�moire. N�anmoins, l'ex�cution de certaines
instructions, ou certains �v�nements peuvent modifier ce flux:
\begin{itemize}
\item En cas de saut.
\item En cas d'appel ou de retour de proc�dure.
\item En cas de \textit{trap}.
\item En cas d'interruption.
\end{itemize}
Les sauts et appels/retours de proc�dure ont d�j� �t� �tudi�s. Ce sont
des modifications <<~volontaires~>> du flux d'ex�cution. Les
\textit{traps} et interruption sont par contre des modifications en
g�n�ral <<~involontaires~>> de ce flux.

\section{Les \textit{traps}}
Un \textit{trap} (pi�ge en anglais) est un appel de proc�dure que le
processeur d�clenche pour capturer un �v�nement particulier. Par
exemple, si le r�sultat d'une op�ration produit un nombre en virgule
flottante en \textit{overflow} ou en \textit{underflow}, le processeur
r�alise un appel � une proc�dure bien d�finie, appel�e \textit{trap
  handler}, qui g�rera le probl�me potentiel de la fa�on la plus
ad�quate. D'autres exemples d'�v�nements sont:

\begin{itemize}
\item un \textit{opcode} non-d�fini: cette technique �tait � une
  certaine �poque utilis�e pour permettre � des processeurs moins
  puissants d'ex�cuter des programmes �crits pour des machins avec un
  grand jeu d'instructions. Par exemple, certains processeurs
  n'avaient pas d'instructions pour les nombres en virgule
  flottante. Quand une telle instruction devait �tre ex�cut�e, un
  \textit{trap} avait lieu, et le \textit{trap handler} se chargeait
  d'effectuer le calcul en virgule flottante � l'aide des autres
  instructions disponibles (ce qui �tait bien s�r beaucoup plus lourd
  que d'avoir une instruction d�di�e).
\item Une division par z�ro.
\item Un acc�s m�moire � une adresse qui n'existe pas
\end{itemize}

La difficult� est de restaurer le programme qui a �t� interrompu par
le \textit{trap} dans l'�tat o� il se trouvait au moment o� le
\textit{trap} a eu lieu. \textit{A priori}, le programme ne s'attend
pas � ce qu'un \textit{trap} ait lieu, et il ne peut donc pas pr�voir
de sauvegarder les informations importantes au bon moment.

Pour que le programme puisse continuer � s'ex�cuter de mani�re
<<~normale~>>, il faut restaurer \emph{tous} les registres dans l'�tat
o� ils se trouvaient au moment du \textit{trap} \emph{� l'exception}
des registres qui sont cens�s accueillir le r�sultat de
l'op�ration. Par exemple, si le \textit{trap} est caus� par une
op�ration de somme en virgule flottante (o� \verb-R1-, \verb-R2- et
\verb-R3- sont des registres)
\begin{verbatim}
  R1 := R2 + R3
\end{verbatim}
il faut qu'une fois le \textit{trap handler} ex�cut�, \verb-R3- et
\verb-R2- contiennent la valeur qu'ils avaient au moment du
\textit{trap}, mais que \verb-R1- contienne la valeur calcul�e par le
\textit{trap}. Malheureusement, cette op�ration de calcul aura
vraisemblablement utilis� les registres \verb-R2- et \verb-R3- et les
aura donc modifi�es.

Comme dans le cas des appels de proc�dure, il faut donc utiliser un
\textit{stack} (appel� ici \textit{stack syst�me}) pour sauvegarder
les valeurs des registres. Ensuite, la restauration des registres doit
se faire de mani�re s�lective. Une technique souvent utilis�e consiste
� �crire les valeurs � renvoyer dans la copie des registres, et � tout
restaurer. Dans notre exemple cela donne:
\begin{itemize}
\item Sauvegarder tous les registres sur le \textit{stack}
\item Calculer la somme de \verb-R2- et \verb-R3- dans un registre
  \verb-R- (pas forc�ment \verb-R1-).
\item Remplacer la sauvegarde de \verb-R1- sur le \textit{stack} par
  la valeur dans \verb-R-.
\item Restaurer tous les registres � partir du \textit{stack}.
\end{itemize}

\section{Les interruptions}
Quand le processeur veut interroger un p�riph�rique (par exemple lire
des donn�es sur un disque sur), il doit:
\begin{enumerate}
\item Envoyer l'ordre de lecture au p�riph�rique
\item Attendre que le p�riph�rique ait fini sa lecture
\item R�cup�rer les donn�es lues par le p�riph�riques (celui-ci les
  aura �crites sur le bus) et les placer en m�moire.
\end{enumerate}
Le processeur peut alors commencer � traiter les donn�es

Toute cela consomme beaucoup de temps processeur car:
\begin{enumerate}
\item Le p�riph�rique est <<~plus lent~>> que le processeur: il faut
  plus de temps au p�riph�rique pour fournir un octet de donn�es qu'il
  n'en faut au processeur pour le traiter.
\item Le processeur passe du temps � copier les donn�es en m�moire.
\end{enumerate}

Pour r�soudre le point 2., on peut utiliser le m�canisme d'acc�s direct
en m�moire, comme nous l'avons vu au d�but du cours. On a alors la
s�quence suivante:
\begin{enumerate}
\item Le processeur envoie l'ordre de lecture au p�riph�rique
\item Le p�riph�rique lit les donn�es et les �crit en m�moire, pendant
  ce temps le processeur attend.
\end{enumerate}
Une fois ce travail termin�, les donn�es sont d�j� en m�moire et le
processeur peut traiter les donn�es.

N�anmoins, le processeur doit encore attendre le p�riph�rique. Il
serait plus pratique que le processeur puisse lancer l'ordre de
lecture au p�riph�rique, et continuer � ex�cuter un autre
traitement. Dans ce cas, il faudrait que le p�riph�rique soit capable
de \emph{signaler} au processeur qu'il a fini son traitement, pour
\emph{interrompre} ce dernier dans son travail en cours. Le processeur
devra alors ex�cuter une \emph{routine de traitement} pour exploiter
l'information produite par le p�riph�rique, avant de revenir \emph{au
  point o� il avait �t� interrompu}. Ce m�canisme est appel�
\emph{m�canisme d'interruption}, et est pr�sent sur toutes les
machines modernes.

La d�roulement d'une interruption peut �tre sch�matis� comme suit:
\begin{enumerate}
\item Le p�riph�rique (ou son contr�leur) qui d�sire �mettre une
  interruption met � 1 une ligne du bus syst�me. On parle alors de
  \emph{demande d'interruption} ou IRQ (pour \textit{interrupt
    request}).
\item Le CPU d�tecte ce signal. Cela doit se faire le plus t�t
  possible, ce qui signifie que, typiquement, le CPU va regarder les
  lignes d'interruption \emph{� chaque tour de la boucle
    d'interpr�tation}. Le CPU d�termine qui a �mis l'interruption car
  il peut y avoir plusieurs p�riph�riques qui sont capables de
  d�clencher une IRQ, et le traitement n'est pas forc�ment le m�me.
  Cette �tape peut donner lieu � un dialogue entre le CPU et le
  p�riph�rique.
\item Le CPU sauvegarde PC et certains registres (les registres de
  contr�le essentiellement -- pas les registres de travail) sur le
  \textit{stack}, car il va arr�ter d'ex�cuter le programme en cours
  et commencer � ex�cuter le \textit{gestionnaire d'interruption}. En
  g�n�ral, on choisira �galement de passer la machine en \emph{mode
    ma�tre} � ce moment-l�, puisque les interruptions servent
  typiquement � communiquer avec les p�riph�riques, ce qui requiert le
  mode ma�tre. 
\item Le CPU modifie le registre PC de mani�re � ce qu'il commence �
  ex�cuter le gestionnaire d'interruption. Pour ce faire, il place
  dans PC l'adresse de la premi�re instruction de ce gestionnaire, qui
  aura �t� charg� en m�moire pr�alablement. Cette adresse est obtenue
  � l'aide d'une table que le CPU conna�t et qui doit �tre charg�e en
  m�moire (son adresse de d�but est en g�n�ral stock�e dans un
  registre d�di�).
\end{enumerate}

Ensuite, le gestionnaire d'interruption commencera � �tre ex�cut�. Il
devra:
\begin{enumerate}
\item Sauvegarder les autres registres (de travail) si n�cessaire.
\item D�terminer, si n�cessaire, quel p�riph�rique a caus�
  l'interruption.
\item Traiter l'interruption
\item Restaurer les registres de travail sauv�s
\item Ex�cuter une instruction sp�ciale du processeur, g�n�ralement
  appel�e \emph{Return from interrupt}, qui va restaurer le registre
  PC et les registres de contr�le, et remettre la machine en mode
  esclave.
\end{enumerate}

\begin{figure}
  \centering
  \includegraphics[scale=.8]{../images/Chap4/boucleint.pdf}
  \caption{La boucle d'interpr�tation du CPU.}
  \label{fig:boucleint}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[scale=.8]{../images/Chap4/boucleintavecint.pdf}
  \caption{La boucle d'interpr�tation du CPU avec gestion des interruptions.}
  \label{fig:boucleintinter}
\end{figure}

Que se passe-t-il si une demande interruption a lieu durant le
traitement d'un gestionnaire d'interruption ? G�n�ralement, on fera en
sorte que celle-ci soit ignor�e jusqu'� la fin du traitement. Il
existe pour ce faire un bit dans un registre de contr�le qui indique
si la machine est interruptible ou non\footnote{Cette information est
  parfois coupl�e au mode: on d�cide que la machine n'est jamais
  interruptible en mode ma�tre.}. Ce bit est mis � $0$ par le CPU
avant d'entrer dans la routine de gestion d'interruption (voir
Fig.~\ref{fig:boucleintinter2}).
\begin{figure}
  \centering
  \includegraphics[scale=.8]{../images/Chap4/boucleintavecint2.pdf}
  \caption{La boucle d'interpr�tation du CPU avec gestion des
    interruptions et machine non-interruptible en cas d'interruption.}
  \label{fig:boucleintinter2}
\end{figure}

Dans ce cas, l'instruction \emph{Return from interrupt} doit aussi
avoir pour effet de rendre la machine � nouveau interruptible. Tout
cela (retour de l'interruption, remise en mode interruptible et remise
ne mode ma�tre) doit se faire en une instruction, sinon on risque
d'�tre interrompu juste avant le retour !

\begin{figure}
  \centering
  \includegraphics[scale=.8]{../images/Chap4/intex1.pdf}
  \caption{Exemple d'interruption (1).}
  \label{fig:exint1}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[scale=.8]{../images/Chap4/intex2.pdf}
  \caption{Exemple d'interruption (2).}
  \label{fig:exint2}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[scale=.8]{../images/Chap4/intex2bis.pdf}
  \caption{Exemple d'interruption (3).}
  \label{fig:exint2bis}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[scale=.8]{../images/Chap4/intex3.pdf}
  \caption{Exemple d'interruption (4).}
  \label{fig:exint3}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[scale=.8]{../images/Chap4/intex4.pdf}
  \caption{Exemple d'interruption (5).}
  \label{fig:exint4}
\end{figure}

Les Fig~\ref{fig:exint1} � \ref{fig:exint4} pr�sentent un exemple
d'interruption.
\begin{enumerate}
\item � la Fig~\ref{fig:exint1}, on vient d'ex�cuter l'instruction �
  l'adresse 256. Avant d'ex�cuter l'instruction en 257, le CPU d�tecte
  une IRQ num�ro 1.
\item Le CPU modifie donc PC en fonction des infos dans le vecteur
  d'interruption. Les registres sont sauv�s sur le \textit{stack}. La
  situation est alors celle de la Fig~\ref{fig:exint2}.
\item Au cours de l'ex�cution du \textit{handler}, les registres
  peuvent �tre modifi�s, voir Fig.~\ref{fig:exint2bis}.
\item Apr�s quelques instructions, on s'appr�te � ex�cuter
  l'instruction � l'adresse 2052, qui est la derni�re du
  \textit{handler} (IRET), comme illustr� � la
  Fig~\ref{fig:exint3}. Les valeurs des registres de travail ont d�j�
  �t� restaur�es, il reste � revenir dans le programme utilisateur.
\item Apr�s ex�cution de cette instruction, on retrouve la valeur de
  PC qui avait �t� sauvegard�e, et on continue � ex�cuter le programme
  utilisateur l� o� il avait �t� interrompu, comme illustr� � la
  Fig.~\ref{fig:exint4}.
\end{enumerate}


Une autre solution consiste � associer des priorit�s aux
interruptions: d�s qu'on entre dans le \textit{handler} d'une
interruption, on en d�sactive toute une s�rie d'autres qui sont
consid�r�es comme moins prioritaires. Il faut donc remplacer le bit
indiquant si la machine est interruptible par un vecteur qui indique
quelles interruptions sont activ�es.

\paragraph{Exemple} Intel 8259: voir documents sur l'UV.


\end{document}




