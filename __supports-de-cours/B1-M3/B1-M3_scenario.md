# Scénario du bloc B1, module M3

## Séquence 1 : Introduction 

Dans ce module nous allons manipuler des données stockées dans des fichiers texte : ce sont les données en table. 

Le module s'articule entre :

- des explications écrites à propos :
    - des formats de fichiers : texte _vs_ binaire, csv
    - des moyens de lire et charger dans un script les données stockées en table
    - des opérations que l'on peut faire sur ces données
    - des facilités offertes par les modules csv et pandas (sans entrer dans l'étude détaillée de ceux-ci)
- des vidéos qui se focalisent sur les traitements de données en table ; ces vidéos peuvent être visionnées indépendemment des textes


1. Vidéo `NSI-B1-M3-S1.mp4`
2. Fiche Vocabulaire des données en table `bloc_1_section_3_unite_0.md`
3. Fiche Tableur, format binaire, format texte  `bloc_1_section_3_unite_1.md`
4. Quiz Fichier, Formats, Données en Table
5. Exercice Sur lecture d'un fichier texte 
6. Exercice Charger des Données en Table 1
7. Fiche : les traitements  `bloc_1_section_3_unite_2.md`
8. Fiche : les modules csv et pandas `bloc_1_section_3_unite_3.md`

## Séquence 2 : Recherche dans une table

1. Vidéo `NSI-B1-M3-S2.mp4`
2. Quiz à propos de la vidéo Recherche
3. Exercice recherche dans une table
4. Exercice recherche dans une table 2

## Séquence 3 : Trier des données

1. Vidéo `NSI-B1-M2-S3.mp4`
2. Quiz à propos de la vidéo Tri
3. Exercice Tri de données 1
4. Exercice Tri de données 2

## Séquence 4 : Fusion de tables

1. Partie 1 : Vidéo `NSI-B1-M3-S4-a1.mp4`
2. Partie 2 : Vidéo `NSI-B1-M3-S4-a2.mp4`
3. Quiz sur les vidéos Fusion
4. Exercice sur la fusion de table

## Séquence 5 : Sauvegarde des données

1. Vidéo : `NSI-B1-M3-S4-a3.mp4`
2. Quiz sur la vidéo Sauvegarde
3. Exercice Sauvegarde de données

## Séquence 6 : Jointure

1. Vidéo : `NSI-B1-M3-S4-b.mp4`
2. Quiz sur la vidéo jointure
3. Exercice Jointure

