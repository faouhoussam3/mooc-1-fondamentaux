.. CBD documentation master file, created by
   sphinx-quickstart on Thu Dec 19 14:34:33 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. role:: raw-latex(raw)
    :format: latex

Cours de bases de données - Aspects systèmes
============================================

Contents:

.. toctree::
   :maxdepth: 3
   :numbered: 1
   
   intro
   stock
   arbreb
   hachage
   moteurs
   opalgo
   optim
   tp-optim
   transactions
   conc
   rp
   annales

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

