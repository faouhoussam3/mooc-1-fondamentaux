1
00:00:00,854 --> 00:00:03,273
Nous nous retrouvons pour une autre vidéo

2
00:00:03,373 --> 00:00:06,646
où nous allons continuer notre projet d'escape game

3
00:00:06,746 --> 00:00:08,695
avec nos objets,

4
00:00:08,795 --> 00:00:11,115
notre modélisation orientée objet.

5
00:00:12,491 --> 00:00:15,483
Je vous propose de retrouver notre programme.

6
00:00:15,583 --> 00:00:17,748
Nous avions déjà créé

7
00:00:17,848 --> 00:00:20,123
les trois objets principaux,

8
00:00:20,223 --> 00:00:21,574
à savoir le maître de jeu,

9
00:00:21,674 --> 00:00:22,927
je vous remontre la structure,

10
00:00:23,027 --> 00:00:24,112
le maître de jeu

11
00:00:24,212 --> 00:00:26,342
qui est le contrôleur de notre programme,

12
00:00:28,457 --> 00:00:30,844
l'objet château et l'objet héros

13
00:00:30,944 --> 00:00:32,683
qui sont les deux objets principaux

14
00:00:32,783 --> 00:00:33,908
de notre modèle,

15
00:00:34,008 --> 00:00:36,646
avec les tortues

16
00:00:36,746 --> 00:00:39,269
pour faire l'interaction avec l'utilisateur.

17
00:00:41,642 --> 00:00:43,026
Nous allons voir maintenant

18
00:00:43,514 --> 00:00:44,953
quelques méthodes et notamment

19
00:00:46,027 --> 00:00:49,699
le début de l'entrée dans ce programme.

20
00:00:49,799 --> 00:00:51,168
Je vous rappelle que l'entrée,

21
00:00:51,268 --> 00:00:52,477
c'est le maître de jeu

22
00:00:52,577 --> 00:00:54,583
et donc le programme principal va consister,

23
00:00:54,683 --> 00:00:55,694
dans un premier temps,

24
00:00:55,794 --> 00:00:57,611
à créer effectivement ce maître de jeu.

25
00:00:59,306 --> 00:01:01,553
Une fois le maître de jeu créé,

26
00:01:01,653 --> 00:01:04,604
on va probablement devoir lancer

27
00:01:04,820 --> 00:01:07,724
ce que j'appelle un setup, un paramétrage,

28
00:01:07,824 --> 00:01:10,470
ne serait-ce que pour récupérer les informations

29
00:01:10,570 --> 00:01:12,897
du château, des portes, des objets, et cætera.

30
00:01:12,997 --> 00:01:14,988
Puis nous allons lancer

31
00:01:15,088 --> 00:01:16,611
effectivement le jeu.

32
00:01:18,184 --> 00:01:20,198
Voilà, et c'est tout pour le programme principal.

33
00:01:20,425 --> 00:01:23,377
Nous allons détailler un petit peu le setup

34
00:01:23,477 --> 00:01:25,553
voir ce qu'il faut faire

35
00:01:26,191 --> 00:01:28,961
pour mettre en place les différents éléments.

36
00:01:30,706 --> 00:01:33,784
Donc la méthode setup du maître de jeu

37
00:01:33,884 --> 00:01:34,912
va consister en quoi ?

38
00:01:35,012 --> 00:01:36,998
D'abord récupérer les fichiers

39
00:01:37,884 --> 00:01:40,399
d'une façon ou d'une autre, il va falloir récupérer

40
00:01:41,336 --> 00:01:42,736
les fichiers de données.

41
00:01:45,407 --> 00:01:47,036
Quels sont ces fichiers de données ?

42
00:01:47,136 --> 00:01:49,295
Je vais leur donner des noms déjà

43
00:01:49,963 --> 00:01:51,516
un fichier plan

44
00:01:53,103 --> 00:01:55,116
alors, la façon dont on récupère

45
00:01:55,316 --> 00:01:56,906
ces fichiers varie,

46
00:01:57,006 --> 00:01:58,463
moi, par exemple, dans ma version,

47
00:01:58,563 --> 00:02:00,649
j'ai utilisé un module qui s'appelle argparse

48
00:02:00,749 --> 00:02:01,695
et qui permet de

49
00:02:01,795 --> 00:02:04,272
traiter les options passées à la ligne de commande

50
00:02:04,384 --> 00:02:05,831
mais on pourrait aussi

51
00:02:05,931 --> 00:02:08,572
utiliser le module sys ou autre.

52
00:02:09,772 --> 00:02:11,829
Donc je ne vais pas détailler plus avant

53
00:02:11,929 --> 00:02:16,844
comment est-ce qu'on récupère effectivement les fichiers.

54
00:02:16,944 --> 00:02:20,033
Chacun est libre de choisir

55
00:02:20,533 --> 00:02:22,243
la solution qui lui convient.

56
00:02:22,343 --> 00:02:24,739
En tout cas, il faut récupérer un fichier plan,

57
00:02:24,839 --> 00:02:27,110
un fichier de portes

58
00:02:29,687 --> 00:02:32,306
et un fichier d'objets.

59
00:02:37,844 --> 00:02:40,493
Ici, par exemple, si on regarde le plan du château,

60
00:02:40,593 --> 00:02:42,373
voilà comment est modélisé le château,

61
00:02:42,473 --> 00:02:43,606
un simple fichier texte

62
00:02:43,706 --> 00:02:45,713
avec des entiers 0, 1, 2, 3, et cætera,

63
00:02:45,813 --> 00:02:47,440
chaque code correspond

64
00:02:47,540 --> 00:02:49,530
à un type de cellule.

65
00:02:49,630 --> 00:02:52,948
0 pour les cellules de vides, de couloirs,

66
00:02:53,048 --> 00:02:55,303
1 pour les cellules de murs,

67
00:02:55,503 --> 00:02:58,020
5, on constate que c'est l'entrée,

68
00:02:58,120 --> 00:02:59,977
ici en haut à gauche

69
00:03:00,077 --> 00:03:01,953
c'est l'endroit où on a démarré,

70
00:03:02,767 --> 00:03:03,767
et cætera.

71
00:03:04,735 --> 00:03:08,377
Et donc ce fichier, on doit effectivement le récupérer

72
00:03:08,477 --> 00:03:10,294
et c'est ce fichier qu'on va passer

73
00:03:12,500 --> 00:03:15,163
à une méthode, au setup du château

74
00:03:15,263 --> 00:03:16,785
pour que le château puisse

75
00:03:17,247 --> 00:03:18,638
puisse se mettre à jour.

76
00:03:18,738 --> 00:03:21,790
Donc récupération des fichiers.

77
00:03:22,261 --> 00:03:24,198
On a évidemment

78
00:03:24,298 --> 00:03:27,654
tout un paramétrage, un réglage à faire

79
00:03:27,754 --> 00:03:30,095
sur les différentes tortues de notre jeu.

80
00:03:30,195 --> 00:03:32,430
Pour ceux qui connaissent un petit peu le module turtle

81
00:03:32,530 --> 00:03:33,631
dès que vous créez une tortue,

82
00:03:33,731 --> 00:03:35,415
elle apparaît sous forme d'une petite flèche

83
00:03:35,515 --> 00:03:36,285
à l'écran.

84
00:03:36,385 --> 00:03:37,473
Évidemment, il va falloir par exemple

85
00:03:37,573 --> 00:03:38,818
faire disparaître cette petite flèche

86
00:03:38,918 --> 00:03:39,083
et cætera.

87
00:03:39,183 --> 00:03:40,361
Bref, on a un certain nombre

88
00:03:40,947 --> 00:03:42,994
de paramétrages des tortues

89
00:03:44,408 --> 00:03:45,422
à faire

90
00:03:46,196 --> 00:03:47,280
qu'on ne va pas détailler.

91
00:03:51,735 --> 00:03:52,992
Je le signale juste.

92
00:03:54,188 --> 00:03:55,396
Et puis on va

93
00:03:57,427 --> 00:03:58,986
on va lancer

94
00:03:59,086 --> 00:04:00,730
ce que j'appelle les autres setups

95
00:04:00,830 --> 00:04:01,543
c'est-à-dire

96
00:04:01,643 --> 00:04:03,068
le setup du château

97
00:04:10,123 --> 00:04:12,467
à qui on va évidemment passer

98
00:04:12,567 --> 00:04:13,409
le fichier plan

99
00:04:13,509 --> 00:04:14,395
pour qu'il puisse

100
00:04:14,806 --> 00:04:17,645
mettre à jour

101
00:04:17,745 --> 00:04:19,319
notamment sa liste de cellules.

102
00:04:22,188 --> 00:04:24,220
On va également lui passer

103
00:04:27,306 --> 00:04:30,185
le fichier de portes

104
00:04:30,786 --> 00:04:33,591
puisque les portes vont être des cellules particulières

105
00:04:36,262 --> 00:04:38,392
et on va lui passer le fichier des objets.

106
00:04:41,534 --> 00:04:45,135
Alors, en retour, ce qu'on peut récupérer

107
00:04:45,235 --> 00:04:47,531
c'est les coordonnées de l'entrée

108
00:04:47,631 --> 00:04:50,994
puisque cette case un peu particulière

109
00:04:51,094 --> 00:04:52,117
va nous permettre de

110
00:04:52,576 --> 00:04:55,167
mettre à jour notre héros,

111
00:04:55,267 --> 00:04:56,347
je vous rappelle que le héros

112
00:04:56,823 --> 00:04:58,298
a des coordonnées x, y

113
00:04:58,398 --> 00:04:59,972
qui pour l'instant ne sont pas connues,

114
00:05:00,072 --> 00:05:00,599
qui sont None.

115
00:05:01,394 --> 00:05:03,001
Et donc une fois que le château

116
00:05:03,101 --> 00:05:04,263
a fait ses réglages,

117
00:05:04,363 --> 00:05:05,496
on va pouvoir lancer

118
00:05:06,595 --> 00:05:08,544
les réglages du héros,

119
00:05:09,000 --> 00:05:11,039
notamment en lui passant l'entrée

120
00:05:11,139 --> 00:05:12,651
pour qu'il puisse mettre à jour

121
00:05:13,095 --> 00:05:14,586
ses coordonnées en x et en y.

122
00:05:17,506 --> 00:05:19,285
Voilà, et c'est à peu près tout.

123
00:05:19,385 --> 00:05:20,710
Donc on voit que

124
00:05:21,949 --> 00:05:24,058
ici, les choses sont quand même bien découpées.

125
00:05:25,318 --> 00:05:26,839
On crée les objets,

126
00:05:27,039 --> 00:05:29,716
on lance donc le maître de jeu

127
00:05:29,816 --> 00:05:31,566
l'objet principal, qui est le point d'entrée,

128
00:05:31,666 --> 00:05:33,842
qui lance un certain nombre de réglages.

129
00:05:33,942 --> 00:05:35,808
Et ensuite, il demande

130
00:05:35,908 --> 00:05:38,884
aux autres objets de faire de même.

131
00:05:38,984 --> 00:05:40,142
Donc c'est le maître de jeu

132
00:05:40,242 --> 00:05:42,163
qui lance le paramétrage

133
00:05:42,263 --> 00:05:43,377
en disant au château "vas-y"

134
00:05:44,371 --> 00:05:45,527
"j'ai fini mes réglages"

135
00:05:45,627 --> 00:05:47,628
"à toi de faire les tiens"

136
00:05:47,728 --> 00:05:49,452
Puis il lance les réglages du héros.

137
00:05:49,552 --> 00:05:51,379
Donc ce sont ces interactions

138
00:05:51,479 --> 00:05:53,278
qui permettent petit à petit

139
00:05:53,378 --> 00:05:55,776
au programme de s'exécuter correctement.

140
00:06:00,143 --> 00:06:03,997
Une fois que les réglages sont faits,

141
00:06:04,097 --> 00:06:06,684
on lance la partie,

142
00:06:07,193 --> 00:06:10,601
on va pouvoir écrire un petit peu aussi

143
00:06:10,701 --> 00:06:15,614
le début de cette méthode start.

144
00:06:16,132 --> 00:06:18,543
Quand on lance le jeu, qu'est-ce qu'on doit faire ?

145
00:06:18,643 --> 00:06:20,904
Alors peut-être une petite annonce à l'utilisateur

146
00:06:21,004 --> 00:06:22,855
une sorte de message de bienvenue,

147
00:06:22,955 --> 00:06:24,443
je ne sais pas si vous avez remarqué,

148
00:06:24,543 --> 00:06:26,408
mais effectivement, quand j'ai démarré le jeu,

149
00:06:26,940 --> 00:06:29,364
était affiché ici le but

150
00:06:29,464 --> 00:06:31,491
qui disait de

151
00:06:32,193 --> 00:06:34,155
d'atteindre la sortie.

152
00:06:38,523 --> 00:06:40,311
On peut supposer

153
00:06:41,006 --> 00:06:43,625
vouloir faire un message de bienvenue

154
00:06:47,067 --> 00:06:48,557
par exemple, voilà.

155
00:06:48,957 --> 00:06:50,820
Alors ici,

156
00:06:51,474 --> 00:06:53,769
la variable que j'écris tout en majuscule,

157
00:06:53,869 --> 00:06:55,174
c'est évidemment une constante

158
00:06:55,472 --> 00:06:58,015
et un conseil que je vous donne,

159
00:06:58,115 --> 00:06:59,881
c'est de regrouper

160
00:06:59,981 --> 00:07:01,714
de n'utiliser que des constantes

161
00:07:01,814 --> 00:07:04,185
et de ne pas utiliser d'objets en dur

162
00:07:04,285 --> 00:07:05,859
dans votre code,

163
00:07:05,959 --> 00:07:07,974
et de regrouper par exemple vos constantes

164
00:07:08,074 --> 00:07:09,277
dans un fichier.

165
00:07:10,392 --> 00:07:12,250
Ça facilite la maintenance,

166
00:07:12,415 --> 00:07:14,591
vous savez que toutes vos constantes

167
00:07:14,691 --> 00:07:16,381
sont regroupées dans un fichier,

168
00:07:16,481 --> 00:07:18,682
ça n'alourdit pas non plus du coup

169
00:07:18,782 --> 00:07:20,651
votre fichier principal

170
00:07:20,751 --> 00:07:23,134
et il suffit de faire un import de ce fichier

171
00:07:23,234 --> 00:07:24,143
pour avoir vos constantes.

172
00:07:25,587 --> 00:07:27,403
Donc ici un message de bienvenue.

173
00:07:27,503 --> 00:07:28,813
Ensuite,

174
00:07:32,850 --> 00:07:34,318
il va falloir demander

175
00:07:34,418 --> 00:07:37,054
évidemment c'est un programme graphique,

176
00:07:37,154 --> 00:07:38,105
donc il va falloir demander

177
00:07:38,205 --> 00:07:39,841
aux différents objets de s'afficher,

178
00:07:39,941 --> 00:07:41,945
donc on va demander au château

179
00:07:42,045 --> 00:07:43,403
de s'afficher,

180
00:07:43,503 --> 00:07:45,133
d'initialiser son affichage

181
00:07:46,482 --> 00:07:48,358
init_vue par exemple.

182
00:07:48,458 --> 00:07:50,252
On va demander au héros de s'afficher,

183
00:07:55,403 --> 00:07:56,615
donc je l'appelle update

184
00:07:56,715 --> 00:07:58,409
parce que c'est la même méthode

185
00:07:58,509 --> 00:07:59,223
qu'on va appeler

186
00:07:59,928 --> 00:08:01,255
dès que le héros aura bougé

187
00:08:01,355 --> 00:08:03,644
et qu'il va devoir mettre à jour sa vue.

188
00:08:03,744 --> 00:08:04,812
Dès que le héros bouge,

189
00:08:04,956 --> 00:08:07,096
il faut qu'il se ré-affiche.

190
00:08:09,662 --> 00:08:11,806
Une fois que nos éléments se sont affichés,

191
00:08:13,000 --> 00:08:14,141
qu'est-ce qu'il faut faire ?

192
00:08:14,241 --> 00:08:16,754
Il faut lancer la mécanique de jeu.

193
00:08:17,236 --> 00:08:18,683
On a ici

194
00:08:18,783 --> 00:08:19,977
le lancement

195
00:08:20,177 --> 00:08:21,641
de la gestion des événéments.

196
00:08:21,763 --> 00:08:23,287
Pour l'instant, dans tout ce que nous avons fait,

197
00:08:23,387 --> 00:08:24,341
il n'y a aucun événement,

198
00:08:24,441 --> 00:08:25,768
c'est simplement du statique,

199
00:08:25,868 --> 00:08:26,695
on crée les objets,

200
00:08:26,795 --> 00:08:27,708
on les affiche,

201
00:08:27,923 --> 00:08:29,546
et à partir de maintenant, il va falloir

202
00:08:30,339 --> 00:08:31,854
lancer la mécanique

203
00:08:31,954 --> 00:08:33,015
qui permet de gérer

204
00:08:33,215 --> 00:08:34,656
ce que fait l'utilisateur.

205
00:08:35,034 --> 00:08:37,035
Cette méthode, je l'appelle bind en général,

206
00:08:37,135 --> 00:08:38,064
on peut l'appeler comme on veut,

207
00:08:40,008 --> 00:08:42,584
que nous verrons plus en détail

208
00:08:42,684 --> 00:08:43,777
dans une autre vidéo

209
00:08:45,683 --> 00:08:48,370
que vous devrez peut-être visionner

210
00:08:48,470 --> 00:08:51,245
après avoir vu, après avoir suivi

211
00:08:52,163 --> 00:08:54,711
la section, le module qui parle de

212
00:08:54,811 --> 00:08:56,518
la programmation événementielle,

213
00:08:56,618 --> 00:08:57,990
puisque ce bind

214
00:08:58,090 --> 00:08:58,986
va s'occuper

215
00:08:59,086 --> 00:09:00,315
effectivement des événements.

216
00:09:00,415 --> 00:09:02,649
Mais tout le programme finalement

217
00:09:02,749 --> 00:09:04,173
va se dérouler

218
00:09:04,273 --> 00:09:05,744
derrière ce qu'il se passe

219
00:09:06,004 --> 00:09:06,919
sur cette méthode.

220
00:09:07,019 --> 00:09:08,086
Une fois que le bind est lancé,

221
00:09:10,020 --> 00:09:14,610
il y a quelques actions techniques à faire,

222
00:09:14,710 --> 00:09:17,184
comme en fait, le module turtle

223
00:09:17,284 --> 00:09:18,757
impose donc de lancer une boucle

224
00:09:19,407 --> 00:09:21,282
qui tourne indéfiniment

225
00:09:21,382 --> 00:09:22,847
pour laisser justement

226
00:09:23,476 --> 00:09:25,228
cette fenêtre graphique ouverte

227
00:09:25,328 --> 00:09:29,013
et que les tortues puissent réagir.

228
00:09:29,113 --> 00:09:30,117
Disons que cela par exemple,

229
00:09:30,725 --> 00:09:33,879
c'est l'écran qui gère ça

230
00:09:33,979 --> 00:09:35,679
et donc ici, notre contrôleur

231
00:09:36,150 --> 00:09:38,048
qui va faire appel 

232
00:09:38,148 --> 00:09:40,097
à une méthode du module turtle

233
00:09:40,197 --> 00:09:41,274
qui s'appelle mainloop

234
00:09:41,374 --> 00:09:42,991
et qui permet de garder ouverte

235
00:09:43,091 --> 00:09:44,937
notre interface graphique.

236
00:09:45,460 --> 00:09:47,161
Et grosso modo,

237
00:09:47,261 --> 00:09:48,437
le start ne fait que ça.

238
00:09:48,537 --> 00:09:49,714
Et tout va se passer

239
00:09:49,814 --> 00:09:51,023
derrière cette méthode bind

240
00:09:51,123 --> 00:09:52,377
que nous n'avons pas détaillée

241
00:09:52,477 --> 00:09:54,234
et que nous verrons plus tard.

