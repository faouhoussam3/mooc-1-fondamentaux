
# Source

> Pour rédiger cette partie, nous nous sommes basé sur les supports :
>
> [Yves Roggeman, LANGAGES DE PROGRAMMATION, VOL. I (3ème édition) 2020](https://files.inria.fr/LearningLab_public/C045TV/DOCS-REFERENCE/Roggeman-Langages_I.pdf)
>
> [Yves Roggeman, LANGAGES DE PROGRAMMATION, VOL. II (4ème édition) 2020](https://files.inria.fr/LearningLab_public/C045TV/DOCS-REFERENCE/Roggeman-Langages_II.pdf)

> voir aussi
> https://fr.wikipedia.org/wiki/Paradigme_(programmation)
> http://projet.eu.org/pedago/sin/ICN/1ere/4-langages.pdf

Nous invitons le lecteur à utiliser ces références pour compléter l'information résumée ici.








