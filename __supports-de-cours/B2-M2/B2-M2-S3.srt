1
00:00:00,000 --> 00:00:01,304
Nous résumons maintenant

2
00:00:01,404 --> 00:00:03,342
les caractéristiques des langages impératifs.

3
00:00:03,442 --> 00:00:04,700
Un langage est dit impératif

4
00:00:04,800 --> 00:00:06,799
s'il utilise le paradigme de programmation

5
00:00:06,899 --> 00:00:08,048
impérative

6
00:00:08,148 --> 00:00:10,750
où les opérations sont des séquences d'instructions

7
00:00:10,850 --> 00:00:11,917
exécutées par l'ordinateur

8
00:00:12,017 --> 00:00:13,918
pour modifier l'état du programme.

9
00:00:14,203 --> 00:00:16,317
La plupart des langages impératifs

10
00:00:16,417 --> 00:00:18,873
comportent cinq constructions de base.

11
00:00:19,248 --> 00:00:21,116
La déclaration de variable,

12
00:00:21,216 --> 00:00:23,753
l'assignation, également appelée affectation,

13
00:00:24,006 --> 00:00:25,320
la séquence,

14
00:00:25,420 --> 00:00:26,725
le test et la boucle,

15
00:00:26,825 --> 00:00:28,077
auxquels s'ajoutent généralement

16
00:00:28,177 --> 00:00:29,797
des instructions d'entrée/sortie.

17
00:00:31,764 --> 00:00:34,199
Si nous ajoutons la possibilité d'utiliser

18
00:00:34,299 --> 00:00:36,732
et de définir des fonctions ou des modules,

19
00:00:36,982 --> 00:00:38,434
nous parlons de paradigme

20
00:00:38,534 --> 00:00:40,147
de programmation procédurale,

21
00:00:40,247 --> 00:00:42,559
ou plus simplement, de langages procéduraux.

22
00:00:42,884 --> 00:00:44,877
Dans ce cas, un programme complet

23
00:00:44,977 --> 00:00:46,132
comporte généralement,

24
00:00:46,232 --> 00:00:48,275
dans un ordre qui dépend du langage,

25
00:00:48,375 --> 00:00:49,571
un contexte,

26
00:00:49,671 --> 00:00:50,803
l'identité du programme,

27
00:00:50,903 --> 00:00:52,748
son nom, des éléments informatifs,

28
00:00:52,848 --> 00:00:54,192
auteur, date d'écriture,

29
00:00:54,292 --> 00:00:55,631
machine cible,

30
00:00:55,731 --> 00:00:57,176
l'objet du programme, et cætera,

31
00:00:57,601 --> 00:01:00,325
les importations de différents paquetages

32
00:01:00,425 --> 00:01:02,626
ou bibliothèques standards ou ad hoc,

33
00:01:02,726 --> 00:01:04,825
ainsi que les autres fichiers sources à intégrer,

34
00:01:05,133 --> 00:01:07,244
les données et déclarations globales

35
00:01:07,344 --> 00:01:10,244
partagées par le programme et ses différents sous-programmes,

36
00:01:10,344 --> 00:01:12,763
les codes de sous-programmes éventuels,

37
00:01:14,201 --> 00:01:16,387
et le code principal du programme

38
00:01:16,487 --> 00:01:18,385
qui contient les déclarations, définitions

39
00:01:18,485 --> 00:01:20,318
et initialisation des éléments locaux

40
00:01:20,418 --> 00:01:21,612
et les instructions.

41
00:01:22,521 --> 00:01:24,136
Si l'on compare différents langages,

42
00:01:24,236 --> 00:01:25,949
la plupart des langages algorithmiques

43
00:01:26,049 --> 00:01:28,115
qu'on appelle ALGOL-like,

44
00:01:28,215 --> 00:01:29,653
se ressemblent fort

45
00:01:30,163 --> 00:01:32,970
par la forme et l'effet de leurs instructions

46
00:01:33,070 --> 00:01:34,900
mais ils peuvent sembler très différents

47
00:01:35,000 --> 00:01:37,964
si l'on examine leurs parties contextuelles et déclaratives.

48
00:01:38,237 --> 00:01:39,388
Dans la vidéo suivante,

49
00:01:39,488 --> 00:01:41,570
nous détaillons certaines grandes caractéristiques

50
00:01:41,670 --> 00:01:44,199
qui permettent de classer les langages de programmation.

