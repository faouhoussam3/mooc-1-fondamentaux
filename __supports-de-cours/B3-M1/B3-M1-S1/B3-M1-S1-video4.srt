1
00:00:01,354 --> 00:00:03,081
Nous avons vu jusqu'à présent

2
00:00:03,181 --> 00:00:06,318
comment pouvoir exprimer un algorithme,

3
00:00:06,709 --> 00:00:08,602
se poser des questions sur la façon

4
00:00:08,702 --> 00:00:10,141
dont on va prendre en compte

5
00:00:10,241 --> 00:00:12,101
les opérations qui sont sous-jacentes.

6
00:00:12,437 --> 00:00:15,576
Maintenant, on va tenter dans cette petite vidéo

7
00:00:15,676 --> 00:00:17,071
de faire deux choses,

8
00:00:17,171 --> 00:00:19,794
une première chose, ça va être de situer l'algorithmique

9
00:00:20,095 --> 00:00:21,735
dans le paysage de l'informatique

10
00:00:21,835 --> 00:00:22,798
en tant que science,

11
00:00:23,007 --> 00:00:24,728
de voir comment elle s'intègre,

12
00:00:25,048 --> 00:00:27,389
et de voir, dans un deuxième temps,

13
00:00:27,489 --> 00:00:29,791
une petite partie de bibliographie.

14
00:00:30,272 --> 00:00:31,676
Donc si je reprends

15
00:00:31,776 --> 00:00:34,797
ce sur quoi porte l'informatique,

16
00:00:34,897 --> 00:00:37,357
c'est-à-dire le traitement automatique

17
00:00:37,457 --> 00:00:38,457
de l'information pour résoudre un problème

18
00:00:38,557 --> 00:00:42,394
de nature traitement de l'information,

19
00:00:42,494 --> 00:00:45,919
je vais avoir, initialement,

20
00:00:46,902 --> 00:00:48,143
de l'information

21
00:00:48,741 --> 00:00:50,819
et un problème à résoudre.

22
00:00:51,800 --> 00:00:53,004
Ce problème à résoudre,

23
00:00:53,104 --> 00:00:55,308
ça peut être rechercher un élément dans un tableau,

24
00:00:55,408 --> 00:00:57,526
ça peut être trouver

25
00:00:57,744 --> 00:01:00,225
le meilleur chemin dans un graphe,

26
00:01:00,325 --> 00:01:01,612
ça peut être tout ce que vous voulez.

27
00:01:01,988 --> 00:01:04,495
Nous allons en voir un certain nombre en algorithmique.

28
00:01:05,178 --> 00:01:07,664
Une fois que j'ai le problème qui est donné

29
00:01:07,764 --> 00:01:08,832
avec de l'information,

30
00:01:09,155 --> 00:01:11,489
une première étape consiste à dire

31
00:01:11,723 --> 00:01:13,635
pour traiter ce problème,

32
00:01:13,974 --> 00:01:17,052
je vais représenter mon information

33
00:01:17,152 --> 00:01:18,778
à l'aide de structures,

34
00:01:18,878 --> 00:01:20,437
c'est-à-dire je vais l'organiser,

35
00:01:20,775 --> 00:01:23,109
je mets des éléments dans un tableau,

36
00:01:23,209 --> 00:01:24,733
je mets des éléments dans une liste,

37
00:01:25,917 --> 00:01:28,447
je structure ça comme un tableau de données,

38
00:01:28,547 --> 00:01:29,193
et cætera,

39
00:01:29,293 --> 00:01:30,641
donc ça, c'est la structure de données.

40
00:01:30,979 --> 00:01:32,981
Et là dessus, je vais pouvoir

41
00:01:33,081 --> 00:01:34,408
exécuter un algorithme

42
00:01:34,785 --> 00:01:36,670
donc je vais le concevoir

43
00:01:37,032 --> 00:01:39,408
et cet algorithme va me permettre

44
00:01:39,508 --> 00:01:41,520
de résoudre le problème qui est posé.

45
00:01:41,791 --> 00:01:43,805
Évidemment, je ne suis pas au bout de mes peines

46
00:01:43,905 --> 00:01:46,139
parce qu'une fois que j'ai un algorithme

47
00:01:46,946 --> 00:01:48,454
et une structure de données,

48
00:01:48,792 --> 00:01:50,743
chose importante, il faut que je transpose,

49
00:01:50,843 --> 00:01:51,729
que je traduise,

50
00:01:52,143 --> 00:01:55,423
cet algorithme dans un langage de programmation

51
00:01:56,653 --> 00:01:58,300
qui pourra être compris

52
00:01:58,629 --> 00:01:59,810
par une machine

53
00:02:00,298 --> 00:02:01,889
sur laquelle cela va s'exécuter.

54
00:02:02,207 --> 00:02:03,854
Donc l'étape suivante, c'est

55
00:02:03,954 --> 00:02:05,624
de construire et d'écrire un programme

56
00:02:05,911 --> 00:02:08,103
à partir de l'algorithme

57
00:02:08,203 --> 00:02:09,593
donc ça suppose un langage,

58
00:02:11,428 --> 00:02:13,952
un langage qui peut être interprété ou compilé,

59
00:02:14,052 --> 00:02:16,661
par exemple, si vous regardez Python,

60
00:02:16,761 --> 00:02:18,618
vous avez un interpréteur.

61
00:02:18,954 --> 00:02:21,721
Et puis, associé à ce texte

62
00:02:22,049 --> 00:02:23,062
de programme,

63
00:02:23,338 --> 00:02:24,510
j'ai des données

64
00:02:24,926 --> 00:02:26,781
et les données seront associées

65
00:02:26,881 --> 00:02:28,073
je dirais à des méta-données,

66
00:02:28,173 --> 00:02:30,607
c'est-à-dire je sais comment elles sont structurées,

67
00:02:30,707 --> 00:02:31,951
je sais exactement

68
00:02:32,255 --> 00:02:33,292
où les récupérer.

69
00:02:33,392 --> 00:02:35,137
Par exemple, c'est dans un fichier

70
00:02:35,237 --> 00:02:37,215
et les éléments sont séparés par un point-virgule.

71
00:02:38,747 --> 00:02:40,943
Une fois que j'ai mon programme et mes données,

72
00:02:41,162 --> 00:02:43,093
je vais pouvoir exécuter cela

73
00:02:43,193 --> 00:02:44,154
sur une machine.

74
00:02:44,560 --> 00:02:46,540
Et cette machine, c'est la machine

75
00:02:47,401 --> 00:02:49,252
qui est adaptée, je dirais,

76
00:02:49,352 --> 00:02:50,781
au langage de programmation

77
00:02:51,085 --> 00:02:52,906
et aux outils

78
00:02:53,006 --> 00:02:54,455
qui me permettent de passer

79
00:02:54,767 --> 00:02:56,890
d'une écriture d'un programme

80
00:02:57,181 --> 00:02:58,345
dans un langage

81
00:02:58,445 --> 00:03:00,106
à un code exécutable

82
00:03:00,206 --> 00:03:00,958
sur la machine

83
00:03:01,271 --> 00:03:03,241
et une fois que ma machine

84
00:03:03,341 --> 00:03:04,717
a terminé le travail,

85
00:03:04,817 --> 00:03:07,142
j'obtiens un résultat.

86
00:03:07,499 --> 00:03:08,389
Alors évidemment,

87
00:03:08,489 --> 00:03:10,572
c'est une représentation tout à fait simplifiée.

88
00:03:10,891 --> 00:03:13,485
Là, je n'ai mis des flèches que dans un sens

89
00:03:13,585 --> 00:03:14,836
mais en fait, les flèches

90
00:03:15,126 --> 00:03:16,217
vont dans tous les sens.

91
00:03:16,507 --> 00:03:19,093
En effet, quand je vais définir mon algorithme,

92
00:03:19,193 --> 00:03:20,741
je vais avoir besoin

93
00:03:20,841 --> 00:03:21,958
d'avoir une bonne idée

94
00:03:22,276 --> 00:03:24,914
du langage de programmation qui va être utilisé derrière.

95
00:03:25,252 --> 00:03:27,066
Est-ce qu'il a des opérateurs spécifiques ?

96
00:03:27,166 --> 00:03:28,546
Est-ce qu'il manipule les listes 

97
00:03:28,829 --> 00:03:29,906
de façon native ?

98
00:03:30,006 --> 00:03:31,281
Est-ce qu'il a des tables de hashage,

99
00:03:31,381 --> 00:03:33,300
un dictionnaire ? Et cætera.

100
00:03:33,400 --> 00:03:35,067
Donc je vais avoir besoin d'éléments

101
00:03:35,335 --> 00:03:37,018
liés au langage de programmation.

102
00:03:37,345 --> 00:03:39,356
De la même façon, quand j'écris un programme,

103
00:03:39,456 --> 00:03:41,794
j'ai un langage qui m'est donné

104
00:03:41,956 --> 00:03:43,729
et j'ai une idée quand même

105
00:03:44,032 --> 00:03:46,196
de comment ce programme

106
00:03:46,296 --> 00:03:47,743
va s'exécuter sur la machine.

107
00:03:48,062 --> 00:03:49,671
Est-ce que j'utilise de la mémoire ?

108
00:03:49,771 --> 00:03:52,709
Est-ce que j'utilise du temps CPU ?

109
00:03:52,809 --> 00:03:54,215
Comment j'utilise les ressources ?

110
00:03:54,315 --> 00:03:56,648
Les entrées-sorties le cas échéant ? Et cætera.

111
00:03:57,005 --> 00:03:58,872
Donc j'ai des aller-retours

112
00:03:58,972 --> 00:04:00,564
entre les différentes boîtes

113
00:04:01,281 --> 00:04:02,507
de façon à pouvoir

114
00:04:03,017 --> 00:04:04,831
comprendre le problème et le traiter.

115
00:04:05,310 --> 00:04:06,256
Évidemment,

116
00:04:06,649 --> 00:04:07,431
à travers tout ça,

117
00:04:07,531 --> 00:04:09,529
il faut que le résultat corresponde

118
00:04:09,786 --> 00:04:12,197
au problème que je me suis posé initialement.

119
00:04:12,596 --> 00:04:14,931
Je dois quand même vérifier toute cette chaîne,

120
00:04:15,031 --> 00:04:16,561
qu'elle me donne des choses correctes.

121
00:04:16,911 --> 00:04:19,232
Donc le résultat va directement interagir

122
00:04:19,332 --> 00:04:22,567
avec le premier nuage,

123
00:04:22,667 --> 00:04:24,210
c'est-à-dire information et problème.

124
00:04:25,110 --> 00:04:27,591
Alors, j'ai représenté ça de cette façon-là

125
00:04:27,913 --> 00:04:29,352
pour dire que

126
00:04:29,452 --> 00:04:31,204
si on prend l'informatique,

127
00:04:32,183 --> 00:04:34,068
le domaine informatique sur lequel

128
00:04:34,168 --> 00:04:36,173
les scientifiques se sont mis d'accord,

129
00:04:36,508 --> 00:04:38,431
c'est de dire que tout objet informatique

130
00:04:38,675 --> 00:04:40,581
dispose de quatre facettes.

131
00:04:40,901 --> 00:04:42,799
Donc les facettes d'un objet informatique

132
00:04:42,899 --> 00:04:44,997
correspondent à différentes parties

133
00:04:45,390 --> 00:04:46,331
de cet objet,

134
00:04:46,534 --> 00:04:48,925
et il y a eu un travail

135
00:04:49,025 --> 00:04:50,841
qui a permis de dire : on a identifié

136
00:04:51,087 --> 00:04:52,318
quatre grandes facettes

137
00:04:52,574 --> 00:04:55,357
associées à un objet informatique.

138
00:04:55,682 --> 00:04:57,316
La première facette correspond

139
00:04:57,416 --> 00:04:59,019
à la représentation de l'information.

140
00:04:59,367 --> 00:05:01,456
Donc ça, c'est ma boîte verte

141
00:05:01,987 --> 00:05:02,977
qui correspond à

142
00:05:03,077 --> 00:05:04,918
comment je vais

143
00:05:05,223 --> 00:05:06,431
coder mon information

144
00:05:06,531 --> 00:05:08,122
et quel est le sens que je peux lui donner,

145
00:05:08,877 --> 00:05:10,947
La deuxième boîte algorithmique

146
00:05:11,305 --> 00:05:13,140
va permettre de décrire des méthodes

147
00:05:13,240 --> 00:05:14,534
de traitement de l'information.

148
00:05:14,926 --> 00:05:16,502
Les langages et programmation

149
00:05:16,602 --> 00:05:18,926
vont faire la jonction entre

150
00:05:19,026 --> 00:05:21,270
la machine et l'algorithme

151
00:05:21,595 --> 00:05:23,049
et les données qui sont derrière.

152
00:05:23,338 --> 00:05:25,046
Et finalement, l'architecture,

153
00:05:25,146 --> 00:05:27,664
ça correspondra au type de machine que l'on a,

154
00:05:27,987 --> 00:05:29,700
aux réseaux de communication sous-jacents,

155
00:05:29,800 --> 00:05:31,158
au système d'exploitation.

156
00:05:31,595 --> 00:05:33,676
Et donc en fait, dans cette définition

157
00:05:34,788 --> 00:05:36,674
de l'informatique en tant que science,

158
00:05:36,774 --> 00:05:38,832
on retrouve ces quatre facettes

159
00:05:38,932 --> 00:05:41,446
qui sont dépendantes les unes des autres.

160
00:05:41,847 --> 00:05:43,345
Ce n'est pas comme en physique

161
00:05:43,445 --> 00:05:44,876
où d'un côté vous pouvez avoir

162
00:05:45,298 --> 00:05:46,601
la mécanique du point

163
00:05:46,701 --> 00:05:47,790
et de l'autre côté,

164
00:05:47,890 --> 00:05:48,595
je ne sais pas,

165
00:05:49,018 --> 00:05:50,712
l'électromagnétisme,

166
00:05:51,031 --> 00:05:53,865
qui sont deux domaines de la physique séparés.

167
00:05:54,184 --> 00:05:56,110
Ici, il est très, très difficile

168
00:05:56,210 --> 00:05:57,503
de séparer ces facettes.

169
00:05:57,820 --> 00:05:59,729
On va effectivement les séparer

170
00:05:59,829 --> 00:06:01,001
de façon un peu artificielle

171
00:06:01,370 --> 00:06:03,801
dans nos cours et dans notre façon de faire,

172
00:06:03,901 --> 00:06:05,118
mais il faut bien comprendre

173
00:06:05,218 --> 00:06:06,385
qu'un même objet

174
00:06:06,485 --> 00:06:08,132
va avoir toutes ces facettes

175
00:06:08,232 --> 00:06:09,422
et on pourra se poser des questions

176
00:06:09,522 --> 00:06:12,199
concernant toutes les facettes simultanément.

177
00:06:12,807 --> 00:06:14,028
La référence de base

178
00:06:14,128 --> 00:06:15,984
pour une discussion autour de ce thème,

179
00:06:16,084 --> 00:06:19,260
c'est Les quatre concepts de l'informatique.

180
00:06:19,360 --> 00:06:21,138
C'est un article écrit par Gilles Dowek

181
00:06:21,418 --> 00:06:24,048
qui a été

182
00:06:24,339 --> 00:06:27,288
consulté de façon abondante

183
00:06:27,388 --> 00:06:29,466
pour la conception de tous les programmes

184
00:06:29,797 --> 00:06:34,436
que l'on a, que ce soit NSI, SNT

185
00:06:34,536 --> 00:06:37,010
et même ISN auparavant

186
00:06:38,975 --> 00:06:40,329
et ce qu'il se passe au collège.

187
00:06:40,429 --> 00:06:41,814
Donc c'est cette vision-là

188
00:06:42,113 --> 00:06:43,564
sur laquelle on a l'accord

189
00:06:43,664 --> 00:06:45,252
de toute la communauté informatique

190
00:06:45,533 --> 00:06:47,077
de ce que c'est que l'informatique.

191
00:06:47,332 --> 00:06:48,929
C'est cette vision-là

192
00:06:49,029 --> 00:06:51,070
qui est, je dirais,

193
00:06:51,170 --> 00:06:52,099
dans tous les préambules

194
00:06:52,199 --> 00:06:53,802
de tous les programmes que vous avez

195
00:06:54,158 --> 00:06:55,664
à votre disposition

196
00:06:55,764 --> 00:06:58,209
dans le premier et second cycles.

197
00:06:58,784 --> 00:07:02,283
Donc voilà le cœur de l'informatique,

198
00:07:02,519 --> 00:07:04,085
et l'algorithmique, évidemment,

199
00:07:04,185 --> 00:07:06,188
c'est une de ces quatre parties.

200
00:07:06,469 --> 00:07:08,454
Et on ne pourra pas parler d'un objet informatique

201
00:07:08,554 --> 00:07:09,674
sans parler d'algorithme.

202
00:07:11,233 --> 00:07:12,477
Évidemment, vous pouvez dire,

203
00:07:12,577 --> 00:07:13,521
ah oui, mais en architecture ?

204
00:07:13,731 --> 00:07:15,017
Et bien non, en architecture,

205
00:07:15,117 --> 00:07:16,616
nous avons également des algorithmes

206
00:07:16,879 --> 00:07:18,691
en systèmes, en réseaux,

207
00:07:18,791 --> 00:07:21,030
un protocole est un algorithme

208
00:07:21,130 --> 00:07:21,889
quand on l'énonce.

209
00:07:22,143 --> 00:07:23,501
Quand on dit par exemple que

210
00:07:23,899 --> 00:07:24,943
je vais envoyer un paquet,

211
00:07:25,043 --> 00:07:26,099
je vais attendre la réponse,

212
00:07:26,199 --> 00:07:27,248
l'acquittement, et cætera,

213
00:07:27,400 --> 00:07:28,605
ça, c'est un protocole

214
00:07:28,928 --> 00:07:30,114
mais c'est un algorithme

215
00:07:30,214 --> 00:07:30,991
qui est derrière

216
00:07:31,274 --> 00:07:33,044
et on peut avoir une abstraction

217
00:07:33,144 --> 00:07:33,903
de cet algorithme

218
00:07:35,379 --> 00:07:37,343
qui peut avoir différentes implémentations.

219
00:07:37,993 --> 00:07:39,937
Tout ça, ça vous permet de dire que

220
00:07:40,303 --> 00:07:42,924
l'informatique et l'algorithmique

221
00:07:43,024 --> 00:07:44,475
se situent de cette façon-là.

222
00:07:44,945 --> 00:07:48,243
Alors, pour terminer cette séquence,

223
00:07:48,486 --> 00:07:51,254
je vais donner quelques références bibliographiques

224
00:07:51,552 --> 00:07:52,773
qui correspondent,

225
00:07:52,873 --> 00:07:54,575
je dirais,

226
00:07:54,675 --> 00:07:56,680
à ce que vous devriez avoir vu.

227
00:07:56,893 --> 00:07:59,882
C'est-à-dire il est important d'aller voir ces livres,

228
00:08:00,168 --> 00:08:01,383
de voir comment ils sont faits,

229
00:08:01,483 --> 00:08:02,894
de comprendre comment ils sont organisés,

230
00:08:03,524 --> 00:08:05,663
et d'avoir travaillé

231
00:08:05,763 --> 00:08:06,931
au moins quelques chapitres

232
00:08:07,208 --> 00:08:09,521
parce que vous n'arriverez pas à tout faire

233
00:08:09,621 --> 00:08:10,797
dans ces livres-là,

234
00:08:11,056 --> 00:08:13,022
mais d'avoir travaillé sur quelques parties

235
00:08:13,122 --> 00:08:14,094
de ces livres, ces ouvrages,

236
00:08:14,405 --> 00:08:17,205
c'est vraiment une bonne façon

237
00:08:17,628 --> 00:08:19,164
de comprendre l'algorithmique.

238
00:08:19,724 --> 00:08:21,803
Donc il y a un ouvrage qui sert de référence

239
00:08:21,903 --> 00:08:23,215
de façon internationale,

240
00:08:24,693 --> 00:08:26,616
qui correspond à un cours du MIT,

241
00:08:28,049 --> 00:08:29,349
sur l'algorithmique,

242
00:08:29,449 --> 00:08:30,849
c'est le cours de base

243
00:08:31,130 --> 00:08:32,289
qui est pratiquement suivi

244
00:08:32,389 --> 00:08:34,576
par tous les étudiants d'informatique du globe.

245
00:08:35,046 --> 00:08:37,172
C'est l'ouvrage "Algorithmique",

246
00:08:37,272 --> 00:08:38,869
alors ça, c'est la traduction française,

247
00:08:38,969 --> 00:08:41,607
de Cormen, Leiserson, Rivest et Stein.

248
00:08:42,089 --> 00:08:44,338
Il est mis à jour très régulièrement.

249
00:08:46,296 --> 00:08:47,626
Là, je ne sais pas quelle est l'édition,

250
00:08:47,726 --> 00:08:49,346
ça doit être la cinquième ou la sixième,

251
00:08:49,636 --> 00:08:52,098
et donc, c'est un ouvrage très pédagogique

252
00:08:52,698 --> 00:08:54,653
très verbeux au sens

253
00:08:55,193 --> 00:08:56,454
où il prend beaucoup de temps

254
00:08:56,554 --> 00:08:57,355
pour expliquer

255
00:08:57,933 --> 00:08:59,076
le sens des algorithmes.

256
00:09:00,409 --> 00:09:01,427
Donc ça, c'est l'approche

257
00:09:01,765 --> 00:09:03,415
que l'on a vue dans l'expression d'algorithmes,

258
00:09:03,515 --> 00:09:05,349
un des premiers algorithmes que je vous ai montrés.

259
00:09:06,215 --> 00:09:08,798
Le deuxième ouvrage que je vous propose,

260
00:09:08,898 --> 00:09:11,509
il est en Java

261
00:09:12,917 --> 00:09:15,822
mais vous pouvez trouver en C, ou dans d'autres langages,

262
00:09:16,374 --> 00:09:17,785
c'est "Algorithms"

263
00:09:17,885 --> 00:09:19,270
de Sedgewick et Wayne

264
00:09:21,106 --> 00:09:22,591
qui a le parti pris de dire

265
00:09:22,691 --> 00:09:24,144
pour présenter les algorithmes,

266
00:09:24,244 --> 00:09:25,874
je vais présenter ça

267
00:09:26,161 --> 00:09:28,645
avec des programmes

268
00:09:28,990 --> 00:09:30,924
qui représentent les algorithmes

269
00:09:31,282 --> 00:09:32,513
et je vais écrire mes programmes

270
00:09:32,613 --> 00:09:34,225
de façon suffisamment claire

271
00:09:34,444 --> 00:09:37,251
pour que la transposition dans un autre langage de programmation

272
00:09:37,582 --> 00:09:38,524
puisse se faire.

273
00:09:38,778 --> 00:09:40,096
Je vous avais montré ça également

274
00:09:40,196 --> 00:09:42,050
dans la partie Expression d'algorithmes.

275
00:09:42,596 --> 00:09:44,293
Ces deux ouvrages, à mon avis,

276
00:09:44,393 --> 00:09:45,432
il est intéressant

277
00:09:45,662 --> 00:09:46,917
d'aller au moins les voir,

278
00:09:47,017 --> 00:09:47,908
voir comment ils sont faits

279
00:09:48,090 --> 00:09:49,395
de façon à

280
00:09:49,653 --> 00:09:51,993
prendre un peu de recul par rapport à la discipline.

281
00:09:52,764 --> 00:09:54,911
Alors j'ai quelques ouvrages plus avancés

282
00:09:55,223 --> 00:09:57,043
que j'utilise moi pour mes cours.

283
00:09:57,143 --> 00:10:01,356
Ce sont déjà des ouvrages plus complexes

284
00:10:01,806 --> 00:10:03,637
souvent elliptiques

285
00:10:03,737 --> 00:10:06,333
c'est-à-dire que seules les informations importantes

286
00:10:06,433 --> 00:10:07,406
sont présentes,

287
00:10:07,674 --> 00:10:09,983
et donc il faut remplir les trous entre les lignes.

288
00:10:10,836 --> 00:10:12,095
Celui que j'aime beaucoup, c'est

289
00:10:12,195 --> 00:10:14,670
"The Design and Analysis of Algorithms"

290
00:10:14,900 --> 00:10:16,269
par Kozen.

291
00:10:16,555 --> 00:10:17,783
C'est un ouvrage assez ancien,

292
00:10:17,883 --> 00:10:19,126
des années 90,

293
00:10:19,390 --> 00:10:21,457
et qui présente ça sous forme de leçons

294
00:10:21,672 --> 00:10:23,560
mais déjà on est sur de l'algorithmique

295
00:10:23,660 --> 00:10:24,913
un petit peu avancée.

296
00:10:25,928 --> 00:10:27,375
Un deuxième ouvrage

297
00:10:27,475 --> 00:10:29,250
que je trouve intéressant à regarder,

298
00:10:29,350 --> 00:10:32,304
c'est l'ouvrage de Harel et Feldman

299
00:10:32,628 --> 00:10:33,991
de 2004,

300
00:10:34,091 --> 00:10:37,110
qui s'appelle "Algorithmics : The Spirit of Computing"

301
00:10:37,457 --> 00:10:39,282
et cet ouvrage-là

302
00:10:39,382 --> 00:10:41,141
prend vraiment une autre façon

303
00:10:41,771 --> 00:10:43,412
de décrire les algorithmes,

304
00:10:43,512 --> 00:10:45,202
de les présenter

305
00:10:45,302 --> 00:10:47,168
et de présenter les principes, et cætera.

306
00:10:47,425 --> 00:10:49,458
Donc je pense que c'est un autre point de vue

307
00:10:49,558 --> 00:10:50,630
qui est tout à fait intéressant.

308
00:10:51,779 --> 00:10:53,251
Le troisième ouvrage

309
00:10:53,444 --> 00:10:57,401
est plutôt orienté analyse d'algorithmes.

310
00:10:58,734 --> 00:11:00,225
C'est une référence

311
00:11:00,325 --> 00:11:02,643
mais c'est un ouvrage qui est très mathématique

312
00:11:04,626 --> 00:11:06,682
qui est plutôt difficile d'accès

313
00:11:06,782 --> 00:11:07,805
donc je pense que c'est plutôt

314
00:11:07,905 --> 00:11:09,794
pour des personnes qui ont

315
00:11:09,894 --> 00:11:11,000
un peu de recul

316
00:11:11,329 --> 00:11:12,273
en mathématiques

317
00:11:13,209 --> 00:11:15,434
mais dans lequel vous avez des analyses de complexité,

318
00:11:15,534 --> 00:11:16,555
et c'est tout à fait moderne,

319
00:11:17,450 --> 00:11:19,359
c'est de l'informatique des années

320
00:11:20,599 --> 00:11:22,234
je dirais 2000,

321
00:11:22,334 --> 00:11:23,875
il y a toute une école qui s'est développée

322
00:11:23,975 --> 00:11:25,441
à partir des années 90,

323
00:11:25,702 --> 00:11:28,585
et c'est vraiment ce genre de choses

324
00:11:28,685 --> 00:11:29,510
qu'on va regarder là.

325
00:11:30,107 --> 00:11:32,043
J'ai un ouvrage un peu plus ancien

326
00:11:32,143 --> 00:11:33,108
que je trouve sympathique

327
00:11:33,829 --> 00:11:36,156
qui s'appelle "Fundamental of Algorithms"

328
00:11:36,573 --> 00:11:38,389
par Brassard et Bratley

329
00:11:38,656 --> 00:11:40,360
où là, il y a plein de petits exemples,

330
00:11:40,933 --> 00:11:42,124
il y a plein de choses dedans,

331
00:11:42,224 --> 00:11:43,611
c'est assez rigolo.

332
00:11:44,566 --> 00:11:45,307
Moi, je l'aime bien

333
00:11:45,407 --> 00:11:48,133
parce qu'il est facile de rentrer dedans

334
00:11:49,525 --> 00:11:53,128
et les auteurs ont un souci pédagogique important.

335
00:11:53,698 --> 00:11:56,129
Et le dernier ouvrage dont je voulais vous parler,

336
00:11:56,486 --> 00:11:58,558
c'est "Randomized Algorithms".

337
00:11:58,857 --> 00:12:00,552
Là, on aura une séance,

338
00:12:00,652 --> 00:12:03,264
je pense, pendant la formation,

339
00:12:03,557 --> 00:12:05,585
qui portera sur les algorithmes

340
00:12:05,685 --> 00:12:06,902
qu'on dit randomisés, c'est-à-dire

341
00:12:07,002 --> 00:12:08,130
que se passe-t-il

342
00:12:08,371 --> 00:12:09,624
si dans mes algorithmes,

343
00:12:09,724 --> 00:12:10,742
je rajoute

344
00:12:11,065 --> 00:12:12,425
un générateur aléatoire ?

345
00:12:13,290 --> 00:12:14,940
Qu'est-ce que je peux faire en plus ?

346
00:12:15,175 --> 00:12:17,038
Comment évoluent les complexités

347
00:12:17,138 --> 00:12:18,880
si j'ai random ? Et cætera, et cætera.

348
00:12:19,199 --> 00:12:21,406
Ça, c'est la référence historique,

349
00:12:21,698 --> 00:12:23,240
je dirais, de ces algorithmes-là.

350
00:12:23,906 --> 00:12:25,067
Tout ça, ça vous donne

351
00:12:25,167 --> 00:12:27,022
quelques ouvrages de référence.

352
00:12:27,122 --> 00:12:28,105
Je pense que c'est intéressant

353
00:12:28,205 --> 00:12:29,604
d'aller les voir et consulter.

354
00:12:30,186 --> 00:12:32,347
Sur Google Books,

355
00:12:32,447 --> 00:12:33,632
vous pouvez en voir des extraits

356
00:12:33,732 --> 00:12:36,552
donc ça peut se faire assez facilement.

357
00:12:37,283 --> 00:12:38,578
Enfin, je voudrais citer

358
00:12:38,678 --> 00:12:40,038
quelques références

359
00:12:40,138 --> 00:12:41,478
de références que j'ai utilisées.

360
00:12:42,380 --> 00:12:44,372
"The Art of Computer Programming"

361
00:12:44,472 --> 00:12:45,475
par Donald Knuth,

362
00:12:45,751 --> 00:12:47,461
qui est vraiment l'ouvrage historique

363
00:12:47,561 --> 00:12:48,702
qu'il faut aller voir.

364
00:12:48,979 --> 00:12:51,005
Il faut aller voir comment il écrit les choses,

365
00:12:51,294 --> 00:12:52,636
il faut essayer de comprendre

366
00:12:52,892 --> 00:12:54,593
et ça vous donne déjà un recul

367
00:12:54,693 --> 00:12:55,939
par rapport à l'évolution

368
00:12:56,039 --> 00:12:58,931
de la discipline informatique

369
00:12:59,031 --> 00:13:00,690
et de l'algorithmique dans cette discipline.

370
00:13:01,352 --> 00:13:02,416
Et le deuxième,

371
00:13:02,516 --> 00:13:04,855
qui est un peu plus récent, je dirais,

372
00:13:05,590 --> 00:13:06,942
d'un point de vue démarche,

373
00:13:07,042 --> 00:13:09,143
c'est "Data Structures and Algorithms"

374
00:13:09,429 --> 00:13:11,443
de Aho, Hopcroft et Ullman,

375
00:13:11,719 --> 00:13:13,618
où là aussi, c'est une façon

376
00:13:13,718 --> 00:13:14,799
de présenter les choses

377
00:13:15,072 --> 00:13:16,722
qui est plus proche de la machine

378
00:13:16,969 --> 00:13:18,581
et qui peut être tout à fait

379
00:13:18,947 --> 00:13:20,144
intéressante à regarder.

380
00:13:20,399 --> 00:13:22,191
Enfin, je cite un ouvrage,

381
00:13:22,522 --> 00:13:25,824
je crois que c'était issu d'un travail d'une IREM,

382
00:13:26,088 --> 00:13:28,374
"Histoires d'algorithmes"

383
00:13:28,474 --> 00:13:31,844
par Jean-Luc Chabert et plein d'autres personnes,

384
00:13:32,146 --> 00:13:33,350
qui donne une idée

385
00:13:33,450 --> 00:13:35,240
sur l'aspect historique

386
00:13:37,854 --> 00:13:39,559
lié aux algorithmes

387
00:13:39,734 --> 00:13:41,999
et des algorithmes qui sont plutôt numériques.

