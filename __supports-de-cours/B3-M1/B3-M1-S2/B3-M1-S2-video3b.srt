1
00:00:01,248 --> 00:00:02,268
Reprenons maintenant

2
00:00:02,550 --> 00:00:04,266
notre calcul de puissance.

3
00:00:05,004 --> 00:00:06,614
L'idée que l'on avait

4
00:00:06,714 --> 00:00:07,992
dans notre calcul de puissance

5
00:00:08,092 --> 00:00:09,303
était de dire on va calculer

6
00:00:09,585 --> 00:00:11,911
x puissance n

7
00:00:12,085 --> 00:00:13,561
et on va dire que c'est tout simplement

8
00:00:13,661 --> 00:00:16,325
x puissance n - 1 fois x.

9
00:00:18,422 --> 00:00:21,911
Donc ça, c'est le cœur de l'algorithme.

10
00:00:22,044 --> 00:00:24,474
C'est donc qu'on va faire une accumulation du résultat.

11
00:00:25,778 --> 00:00:27,865
Maintenant, si je vous demande

12
00:00:28,627 --> 00:00:30,320
de calculer x au carré.

13
00:00:30,420 --> 00:00:32,242
Cela va prendre une opération.

14
00:00:33,775 --> 00:00:36,184
Si j'ai envie de calculer x puissance 4,

15
00:00:36,496 --> 00:00:37,981
combien cela va prendre d'opérations ?

16
00:00:38,757 --> 00:00:41,442
En fait, si je veux calculer x puissance 4,

17
00:00:42,115 --> 00:00:45,220
c'est x au carré

18
00:00:45,320 --> 00:00:49,141
fois x au carré.

19
00:00:51,156 --> 00:00:52,440
Je calcule x au carré,

20
00:00:52,540 --> 00:00:53,644
et puis je l'élève au carré.

21
00:00:54,022 --> 00:00:56,733
Donc j'ai fait deux opérations.

22
00:00:57,180 --> 00:00:59,440
Si je veux calculer x puissance 8,

23
00:00:59,701 --> 00:01:02,000
j'élève x puissance 4 au carré.

24
00:01:02,313 --> 00:01:04,354
Donc j'aurai fait trois opérations.

25
00:01:05,399 --> 00:01:07,024
Il faut vraiment se convaincre

26
00:01:07,124 --> 00:01:09,671
que de faire ça, ça va marcher.

27
00:01:12,257 --> 00:01:14,001
Je fais trois opérations donc

28
00:01:14,101 --> 00:01:15,484
ça veut dire que j'en fais moins

29
00:01:15,584 --> 00:01:17,075
qu'avec le schéma itératif.

30
00:01:17,389 --> 00:01:18,817
Supposons maintenant que j'ai envie

31
00:01:18,917 --> 00:01:20,566
x puissance 2n.

32
00:01:21,567 --> 00:01:22,884
Ça revient à dire

33
00:01:22,984 --> 00:01:24,621
je vais calculer x puissance n

34
00:01:24,980 --> 00:01:26,400
et puis je vais l'élever au carré.

35
00:01:29,311 --> 00:01:31,070
Et si j'ai envie de calculer

36
00:01:31,814 --> 00:01:38,038
x puissance 2n + 1,

37
00:01:38,344 --> 00:01:39,829
qu'est-ce que je vais faire ?

38
00:01:40,450 --> 00:01:44,768
Je vais dire c'est x puissance n

39
00:01:47,218 --> 00:01:50,320
fois x puissance n

40
00:01:55,162 --> 00:01:55,992
fois x.

41
00:01:57,514 --> 00:01:58,503
Là, je réduis

42
00:01:58,891 --> 00:02:00,550
considérablement le nombre d'opérations

43
00:02:01,448 --> 00:02:02,647
puisque je ne vais pas avoir

44
00:02:02,747 --> 00:02:04,187
à faire tous les calculs intermédiaires,

45
00:02:04,287 --> 00:02:05,179
je ne vais pas tout calculer.

46
00:02:05,774 --> 00:02:08,487
Donc en fait, la façon de regarder

47
00:02:09,128 --> 00:02:10,167
ce schéma-là,

48
00:02:10,734 --> 00:02:12,010
ça permet de se dire

49
00:02:12,110 --> 00:02:14,576
là, je vais peut-être pouvoir faire quelque chose

50
00:02:14,992 --> 00:02:18,075
et donc ce que j'ai utilisé comme principe,

51
00:02:18,379 --> 00:02:19,879
qu'on verra en détail par la suite,

52
00:02:20,163 --> 00:02:21,314
c'est ce qu'on appelle un schéma 

53
00:02:21,414 --> 00:02:22,625
de diviser pour régner.

54
00:02:22,929 --> 00:02:24,974
Pour résoudre mon problème de taille n,

55
00:02:25,312 --> 00:02:26,659
j'essaie de trouver

56
00:02:27,527 --> 00:02:30,009
un problème équivalent de taille n/2

57
00:02:30,274 --> 00:02:31,563
et une opération que je fais

58
00:02:31,663 --> 00:02:32,987
sur ce problème de taille n/2.

59
00:02:33,765 --> 00:02:36,153
Là, je passe un peu vite

60
00:02:36,253 --> 00:02:37,164
sur le schéma mais

61
00:02:37,442 --> 00:02:38,489
l'idée, elle est là

62
00:02:38,729 --> 00:02:40,523
d'arriver à faire du diviser pour régner.

63
00:02:41,670 --> 00:02:43,491
Maintenant j'écris mon algorithme.

64
00:02:44,581 --> 00:02:47,555
Donc ici, c'est la puissance diviser pour régner.

65
00:02:49,285 --> 00:02:50,953
Si n égale 0, par convention

66
00:02:51,053 --> 00:02:51,969
on a vu, ça fait 1.

67
00:02:52,624 --> 00:02:54,226
Sinon, je calcule

68
00:02:55,080 --> 00:02:56,293
x puissance n/2.

69
00:02:56,967 --> 00:02:57,892
Si n est pair,

70
00:02:57,992 --> 00:03:01,383
je retourne z fois z.

71
00:03:01,695 --> 00:03:03,527
z, c'est x puissance n/2.

72
00:03:04,069 --> 00:03:04,965
Et si c'est impair,

73
00:03:05,065 --> 00:03:07,700
je retourne z fois z fois x.

74
00:03:11,457 --> 00:03:13,082
Alors, quel est le coût de cet algorithme ?

75
00:03:14,453 --> 00:03:15,658
Et bien, là, je me retrouve

76
00:03:15,758 --> 00:03:17,394
avec quelque chose qui est un peu

77
00:03:17,494 --> 00:03:18,896
plus compliqué que ce qu'on avait avant.

78
00:03:19,247 --> 00:03:21,489
À chaque étape, je divise par 2.

79
00:03:22,316 --> 00:03:23,872
Donc combien je vais avoir d'étapes

80
00:03:24,106 --> 00:03:24,928
dans mon algorithme ?

81
00:03:25,283 --> 00:03:26,413
Chaque fois je divise par 2,

82
00:03:26,513 --> 00:03:28,734
je divise par 2, je divise par 2, je divise par 2.

83
00:03:29,026 --> 00:03:31,734
En gros, je vais avoir à regarder

84
00:03:32,366 --> 00:03:33,881
quelle est la puissance de 2

85
00:03:34,421 --> 00:03:35,633
qui est de l'ordre de n.

86
00:03:36,409 --> 00:03:37,562
Donc en fait, je vais avoir

87
00:03:37,662 --> 00:03:39,021
logarithme en base 2 de n,

88
00:03:40,152 --> 00:03:42,695
c'est exactement le même schéma que pour la dichotomie.

89
00:03:43,150 --> 00:03:44,897
Et à chaque fois je vais passer

90
00:03:45,159 --> 00:03:48,255
soit dans la branche de gauche,

91
00:03:48,355 --> 00:03:50,218
soit dans la branche de droite.

92
00:03:54,091 --> 00:03:55,508
Si je passe dans la branche de gauche,

93
00:03:55,608 --> 00:03:56,552
j'ai une opération,

94
00:03:56,652 --> 00:03:57,727
si je passe dans la branche de droite,

95
00:03:57,827 --> 00:03:58,803
j'ai deux opérations.

96
00:03:59,079 --> 00:04:00,732
Donc en fait, j'ai un encadrement

97
00:04:00,832 --> 00:04:02,337
qui va être immédiat.

98
00:04:02,596 --> 00:04:04,669
Donc en fait, si je regarde bien,

99
00:04:04,769 --> 00:04:05,894
le coût de mon algorithme,

100
00:04:06,146 --> 00:04:07,496
ça va être d'un côté

101
00:04:07,596 --> 00:04:08,881
logarithme en base 2 de n,

102
00:04:09,148 --> 00:04:10,003
et de l'autre côté,

103
00:04:10,103 --> 00:04:11,657
2 logarithme en base 2 de n.

104
00:04:11,999 --> 00:04:14,973
Et donc j'ai un coût qui est en grand O de log n.

105
00:04:15,976 --> 00:04:17,786
La puissance qu'on avait avant,

106
00:04:17,886 --> 00:04:21,221
par exemple, vous vouliez calculer x puissance 1000,

107
00:04:22,625 --> 00:04:24,856
on avait 1000 itérations tout à l'heure,

108
00:04:25,164 --> 00:04:26,680
ici, on n'en a plus que 10.

109
00:04:27,712 --> 00:04:30,332
Donc ça devient extrêmement intéressant.

110
00:04:30,938 --> 00:04:33,461
Alors on peut se dire chouette,

111
00:04:33,561 --> 00:04:36,809
on arrive à une méthode qui marche bien.

112
00:04:38,170 --> 00:04:40,342
Maintenant, est-ce que cette méthode

113
00:04:40,724 --> 00:04:42,821
est la plus efficace possible ?

114
00:04:43,722 --> 00:04:45,488
Alors, on peut se dire oui,

115
00:04:45,588 --> 00:04:48,189
pourquoi pas, elle a l'air vraiment super,

116
00:04:48,289 --> 00:04:50,753
et on va regarder sur des exemples ce que ça donne.

117
00:04:51,505 --> 00:04:53,022
Par exemple, calculer

118
00:04:53,707 --> 00:04:54,831
x puissance 15.

119
00:04:55,947 --> 00:04:57,385
Donc si je prends la méthode standard

120
00:04:57,485 --> 00:04:58,683
que je vous ai donnée tout à l'heure,

121
00:04:59,020 --> 00:05:01,515
je calcule x puissance 7 fois x puissance 7 fois x

122
00:05:01,615 --> 00:05:02,743
puis je calcule x puissance 7

123
00:05:02,843 --> 00:05:05,034
qui est x puissance 3 fois x puissance 3 fois x,

124
00:05:05,134 --> 00:05:05,942
et cætera.

125
00:05:06,042 --> 00:05:08,361
Si je regarde le nombre d'opérations,

126
00:05:08,461 --> 00:05:09,519
j'ai six opérations.

127
00:05:15,653 --> 00:05:17,192
Est-ce que je peux faire moins ?

128
00:05:19,169 --> 00:05:20,105
Oui.

129
00:05:20,205 --> 00:05:21,493
Malheureusement, oui.

130
00:05:22,708 --> 00:05:24,511
Si je veux calculer x puissance 15,

131
00:05:24,611 --> 00:05:27,261
il suffit que je fasse x puissance 5 à la puissance 3,

132
00:05:27,361 --> 00:05:28,786
parce que 15, c'est 3 fois 5.

133
00:05:29,497 --> 00:05:33,089
Pour calculer y qui est égal à x puissance 5,

134
00:05:33,189 --> 00:05:34,382
je vais calculer x au carré

135
00:05:35,243 --> 00:05:36,987
que je vais multiplier par x au carré

136
00:05:37,087 --> 00:05:38,079
et que je vais multiplier par x.

137
00:05:40,421 --> 00:05:44,242
x au carré, ça me donne une opération.

138
00:05:44,715 --> 00:05:46,302
Le calcul de y,

139
00:05:46,402 --> 00:05:48,282
ça me donne deux opérations.

140
00:05:48,925 --> 00:05:51,321
Et puis après,  y, je vais l'élever au cube,

141
00:05:51,421 --> 00:05:52,518
et ça me fait deux opérations.

142
00:05:52,964 --> 00:05:54,257
Au lieu de six opérations

143
00:05:54,357 --> 00:05:55,216
que j'avais ici,

144
00:05:57,830 --> 00:05:58,801
je n'en ai plus que cinq.

145
00:06:00,796 --> 00:06:02,900
Donc la méthode que je vous ai proposée tout à l'heure

146
00:06:03,724 --> 00:06:05,668
n'est pas optimale.

147
00:06:08,878 --> 00:06:11,077
Ça ne marche pas.

148
00:06:12,065 --> 00:06:14,896
Donc j'ai un algorithme

149
00:06:14,996 --> 00:06:16,554
mais je pense que peut-être je peux

150
00:06:16,654 --> 00:06:17,968
améliorer cet algorithme,

151
00:06:18,223 --> 00:06:19,865
puisque mon algorithme ne donne pas

152
00:06:19,965 --> 00:06:23,478
la méthode la plus efficace.

153
00:06:23,792 --> 00:06:26,654
Et donc, se poser la question de la méthode la plus efficace

154
00:06:26,754 --> 00:06:27,753
est quelque chose d'important.

155
00:06:28,527 --> 00:06:30,139
Donc si je prends maintenant

156
00:06:31,837 --> 00:06:33,627
l'algorithme que je viens de donner,

157
00:06:33,949 --> 00:06:38,425
en fait, ça repose sur la décomposition en facteurs de n,

158
00:06:38,525 --> 00:06:40,983
et donc, si n est premier,

159
00:06:41,083 --> 00:06:44,611
je regarde n - 1 qui lui n'est pas premier,

160
00:06:44,919 --> 00:06:46,568
et puis je décompose en facteurs

161
00:06:46,668 --> 00:06:48,630
et si j'ai n qui est égal à p fois q,

162
00:06:48,913 --> 00:06:50,747
je calcule x puissance p

163
00:06:51,070 --> 00:06:52,509
que j'élève après à la puissance q.

164
00:06:53,240 --> 00:06:55,030
Alors effectivement, quel est le coût de cet algorithme ?

165
00:06:55,130 --> 00:06:58,009
Ce n'est pas évident du tout, du tout, du tout à calculer.

166
00:06:59,108 --> 00:07:01,157
Néanmoins, on a un autre algorithme

167
00:07:01,257 --> 00:07:02,798
qui résout le même problème

168
00:07:03,866 --> 00:07:06,502
mais qui, sur des données, a

169
00:07:07,796 --> 00:07:09,564
des valeurs qui sont différentes.

170
00:07:09,881 --> 00:07:12,691
Alors, est-ce que cet algorithme est meilleur ?

171
00:07:13,527 --> 00:07:16,069
Et bien je vous laisse calculer avec cet algorithme

172
00:07:16,169 --> 00:07:19,535
x puissance 33

173
00:07:21,996 --> 00:07:22,818
par exemple,

174
00:07:23,121 --> 00:07:25,042
et là, on se rend compte que, effectivement,

175
00:07:25,142 --> 00:07:27,187
c'est peut-être la méthode classique,

176
00:07:27,465 --> 00:07:29,357
la méthode standard qui va être meilleure.

177
00:07:29,457 --> 00:07:30,447
Je vous laisse calculer.

178
00:07:30,845 --> 00:07:32,797
33, c'est 3 fois 11.

179
00:07:40,032 --> 00:07:43,022
Donc, la question qui se pose,

180
00:07:43,122 --> 00:07:44,473
c'est, étant donné un problème,

181
00:07:45,282 --> 00:07:46,721
est-ce que je peux construire

182
00:07:46,821 --> 00:07:48,519
le meilleur algorithme en matière de complexité

183
00:07:48,619 --> 00:07:49,509
pour résoudre ce problème ?

