1
00:00:01,638 --> 00:00:02,725
Avançons un petit peu

2
00:00:03,013 --> 00:00:07,036
et réfléchissons aux concepts d'itération et de tri.

3
00:00:07,312 --> 00:00:09,401
On a des schémas algorithmiques,

4
00:00:09,501 --> 00:00:11,464
ici, on a deux schémas algorithmiques,

5
00:00:11,564 --> 00:00:13,782
le tri par insertion et le tri par sélection.

6
00:00:14,195 --> 00:00:15,744
Le tri par insertion

7
00:00:15,844 --> 00:00:18,403
va augmenter de 1

8
00:00:18,503 --> 00:00:20,041
l'ensemble des éléments triés

9
00:00:20,141 --> 00:00:21,171
au fur et à mesure

10
00:00:21,382 --> 00:00:22,771
alors que le tri par sélection

11
00:00:22,871 --> 00:00:24,052
va diminuer de 1

12
00:00:24,592 --> 00:00:26,491
l'ensemble des éléments qui restent à trier.

13
00:00:26,895 --> 00:00:30,676
Donc c'est deux façons de voir.

14
00:00:32,248 --> 00:00:33,955
On a vu qu'il y a également

15
00:00:34,055 --> 00:00:37,182
deux manières d'exprimer ces algorithmes,

16
00:00:37,469 --> 00:00:39,178
une façon itérative

17
00:00:39,278 --> 00:00:40,676
sur des indices et cætera,

18
00:00:40,919 --> 00:00:43,466
mais on peut parfaitement les exprimer de façon récursive

19
00:00:43,566 --> 00:00:44,840
pour essayer de dégager

20
00:00:44,940 --> 00:00:47,337
le principe de croissance ou de décroissance d'un ensemble.

21
00:00:47,638 --> 00:00:49,751
Alors attention pour les aspects récursifs,

22
00:00:49,851 --> 00:00:51,241
il faut bien voir à quel moment

23
00:00:51,544 --> 00:00:52,878
on va actualiser notre fonction

24
00:00:52,978 --> 00:00:56,873
et comment les valeurs vont être retournées au fur et à mesure

25
00:00:56,973 --> 00:00:58,817
et remonter au fur et à mesure.

26
00:00:59,717 --> 00:01:02,168
On a vu également dans cet exemple-là,

27
00:01:02,268 --> 00:01:04,159
dans cette partie-là,

28
00:01:04,504 --> 00:01:07,182
comment est-ce qu'on aborde un problème d'algorithmique.

29
00:01:07,436 --> 00:01:08,874
C'est-à-dire quelle est la méthode

30
00:01:08,974 --> 00:01:09,984
qui va être derrière,

31
00:01:10,294 --> 00:01:11,588
quelles spécifications,

32
00:01:11,828 --> 00:01:14,024
comment est-ce qu'on va exprimer notre algorithme,

33
00:01:14,124 --> 00:01:16,306
on peut se permettre d'avoir plusieurs expressions

34
00:01:16,610 --> 00:01:19,091
différentes, par exemple récursif, itératif,

35
00:01:19,378 --> 00:01:21,211
mais on peut aussi avoir d'autres façons de faire.

36
00:01:21,557 --> 00:01:23,581
On travaille sur des exemples.

37
00:01:23,681 --> 00:01:25,464
Ici, j'ai été relativement réduit,

38
00:01:25,564 --> 00:01:27,468
j'ai juste regardé deux exemples,

39
00:01:27,659 --> 00:01:30,165
enfin, un exemple pour chaque algorithme,

40
00:01:30,265 --> 00:01:33,319
mais j'aurais dû faire trois, quatre exemples différents

41
00:01:33,459 --> 00:01:35,617
pour essayer de comprendre comment fonctionnait l'algorithme.

42
00:01:36,233 --> 00:01:38,557
Ensuite, j'ai fait une analyse de cet algorithme,

43
00:01:38,657 --> 00:01:42,072
pour essayer de comprendre et d'évaluer

44
00:01:42,354 --> 00:01:44,070
tout ce qui va être associé.

45
00:01:44,170 --> 00:01:45,307
Et la dernière étape,

46
00:01:45,407 --> 00:01:46,408
que je ne fais pas ici,

47
00:01:46,508 --> 00:01:48,524
et donc qui est plutôt du ressort de la programmation,

48
00:01:48,882 --> 00:01:51,187
c'est comment est-ce que j'implémente cet algorithme

49
00:01:51,392 --> 00:01:53,158
c'est-à-dire le programme Python

50
00:01:53,258 --> 00:01:55,163
qui va aller avec tout ça.

51
00:01:55,516 --> 00:01:57,908
Alors, dans ces familles de tri,

52
00:01:58,164 --> 00:02:00,978
il y a d'autres tris qui sont basés sur les itérations

53
00:02:01,078 --> 00:02:02,825
sur lesquels vous pourrez travailler en exercice,

54
00:02:03,098 --> 00:02:04,730
le tri à bulles par exemple,

55
00:02:04,830 --> 00:02:06,165
le tri cocktail, le tri de Shell

56
00:02:06,265 --> 00:02:07,859
qui est une généralisation du tri par insertion

57
00:02:08,207 --> 00:02:09,635
donc il y a un certain nombre de tris

58
00:02:09,825 --> 00:02:11,309
et d'exercices que l'on peut faire

59
00:02:11,409 --> 00:02:12,327
à partir de là.

