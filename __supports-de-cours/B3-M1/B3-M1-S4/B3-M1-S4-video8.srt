1
00:00:01,155 --> 00:00:03,480
Alors, une fois qu'on a vu ça,

2
00:00:03,580 --> 00:00:05,621
ça, c'est un arbre d'un point de vue général,

3
00:00:05,950 --> 00:00:07,810
on va spécifier notre arbre

4
00:00:07,910 --> 00:00:08,886
et on va dire

5
00:00:09,155 --> 00:00:11,490
je vais regarder des arbres un peu spécifiques

6
00:00:12,035 --> 00:00:15,016
qu'on appelle les arbres binaires.

7
00:00:15,683 --> 00:00:17,294
Donc un arbre binaire,

8
00:00:17,394 --> 00:00:18,788
c'est un arbre dans lequel on a mis

9
00:00:19,303 --> 00:00:21,350
des contraintes supplémentaires

10
00:00:21,450 --> 00:00:22,816
en particulier sur

11
00:00:23,181 --> 00:00:24,283
les fils d'un nœud.

12
00:00:24,810 --> 00:00:28,070
Un nœud ne va pouvoir avoir que deux fils

13
00:00:28,170 --> 00:00:31,403
et en plus, on va spécifier la nature du fils,

14
00:00:31,503 --> 00:00:33,184
si c'est un fils gauche ou si c'est un fils droit.

15
00:00:34,105 --> 00:00:37,467
Pour synthétiser, j'ai une définition récursive.

16
00:00:38,700 --> 00:00:41,045
Un arbre binaire, c'est soit l'arbre vide,

17
00:00:41,378 --> 00:00:43,611
soit un nœud constitué d'une étiquette

18
00:00:43,949 --> 00:00:45,663
et de deux sous-arbres binaires.

19
00:00:46,087 --> 00:00:50,234
Attention, j'ai deux objets, deux types,

20
00:00:50,334 --> 00:00:53,008
j'ai un objet qui s'appelle l'arbre,

21
00:00:53,311 --> 00:00:55,359
et j'ai un objet qui s'appelle nœud.

22
00:00:55,806 --> 00:00:59,108
Et l'arbre est constitué de nœuds

23
00:00:59,429 --> 00:01:00,343
et dans les nœuds,

24
00:01:00,443 --> 00:01:01,867
je vais avoir des arbres.

25
00:01:02,154 --> 00:01:03,891
C'est ça qui fait la récursivité

26
00:01:03,991 --> 00:01:06,497
et qui fait la définition aussi simple.

27
00:01:09,980 --> 00:01:11,710
Ce qu'on a, c'est que tous les nœuds

28
00:01:12,064 --> 00:01:13,562
vont être d'arité 2,

29
00:01:13,662 --> 00:01:16,373
c'est-à-dire qu'il y a toujours deux fils

30
00:01:16,473 --> 00:01:18,427
mais qu'un fils peut être éventuellement vide.

31
00:01:19,172 --> 00:01:21,963
C'est ça l'idée

32
00:01:22,434 --> 00:01:24,446
et c'est sans doute ce qui permet

33
00:01:24,546 --> 00:01:26,953
d'expliquer les choses le plus simplement possible

34
00:01:27,210 --> 00:01:28,680
plutôt que de se poser la question

35
00:01:28,780 --> 00:01:31,348
si j'ai un fils, si j'ai pas de fils, et cætera.

36
00:01:31,679 --> 00:01:32,796
Ici, on a des fils

37
00:01:32,896 --> 00:01:34,610
mais éventuellement le fils est vide.

38
00:01:36,975 --> 00:01:39,152
On a des représentations qui peuvent se faire.

39
00:01:39,475 --> 00:01:40,446
Donc là, je vous donne

40
00:01:40,546 --> 00:01:42,047
une représentation dans laquelle

41
00:01:42,147 --> 00:01:43,736
j'ai représenté

42
00:01:43,836 --> 00:01:45,052
les arbres vides,

43
00:01:45,152 --> 00:01:48,047
ça, c'est un arbre vide,

44
00:01:49,362 --> 00:01:51,873
et je peux éventuellement

45
00:01:51,973 --> 00:01:54,612
si j'ai bien défini mes objets

46
00:01:54,712 --> 00:01:56,096
je peux représenter ça

47
00:01:56,196 --> 00:01:58,428
avec un arbre de cette façon-là

48
00:01:58,725 --> 00:02:00,612
dans lequel j'ai enlevé les arbres vides

49
00:02:00,712 --> 00:02:02,320
qui sont implicites

50
00:02:02,420 --> 00:02:03,699
dans ma représentation.

51
00:02:06,556 --> 00:02:09,515
Alors on appellera feuille dans ce type d'arbre

52
00:02:09,615 --> 00:02:12,053
tout nœud ayant deux sous-arbres vides

53
00:02:12,153 --> 00:02:14,568
donc ici j'ai trois feuilles.

54
00:02:17,116 --> 00:02:20,032
Si je reprends un arbre binaire,

55
00:02:20,132 --> 00:02:24,026
je peux avoir différentes représentations,

56
00:02:24,858 --> 00:02:27,503
et je peux avoir des sous-arbres

57
00:02:27,603 --> 00:02:31,287
qui sont vides à l'intérieur.

58
00:02:32,821 --> 00:02:35,662
Dans arbre dans lequel tous les nœuds sont binaires,

59
00:02:36,049 --> 00:02:38,863
un nœud peut avoir un ou deux sous-arbres vides

60
00:02:38,963 --> 00:02:40,339
et est alors appelé un nœud incomplet.

61
00:02:40,980 --> 00:02:42,521
Sinon un arbre complet

62
00:02:42,621 --> 00:02:44,338
sera un arbre dans lequel

63
00:02:44,438 --> 00:02:46,075
tous les nœuds ont exactement

64
00:02:46,175 --> 00:02:47,569
deux sous-arbres,

65
00:02:47,669 --> 00:02:49,785
éventuellement les deux sont vides

66
00:02:49,885 --> 00:02:51,496
ou aucun des deux n'est vide.

67
00:02:52,222 --> 00:02:53,920
L'autre chose qui est importante,

68
00:02:54,020 --> 00:02:56,462
c'est qu'il faut distinguer le fils gauche du fils droit.

69
00:02:57,931 --> 00:03:00,464
Quand il n'y a qu'un seul fils, non vide,

70
00:03:00,564 --> 00:03:02,741
on doit dire explicitement

71
00:03:02,841 --> 00:03:03,917
s'il est à gauche ou s'il est à droite,

72
00:03:04,588 --> 00:03:07,611
et cette propriété va être extrêmement importante par la suite

73
00:03:07,711 --> 00:03:09,421
en particulier dans les arbres binaires de recherche

74
00:03:09,915 --> 00:03:12,269
puisque la relation fils sera liée à

75
00:03:12,369 --> 00:03:15,837
une comparaison entre les étiquettes de sous-arbres

76
00:03:15,937 --> 00:03:17,771
et l'étiquette du nœud.

77
00:03:20,604 --> 00:03:24,636
Pourquoi est-ce qu'ils jouent un rôle aussi important en informatique ?

78
00:03:27,438 --> 00:03:32,036
En fait, toute décision binaire

79
00:03:32,473 --> 00:03:34,103
la plus simple possible

80
00:03:35,087 --> 00:03:37,169
peut se représenter à l'aide d'un arbre

81
00:03:37,624 --> 00:03:40,369
et si vous avez plusieurs choix dans vos décisions,

82
00:03:40,829 --> 00:03:42,583
vous pouvez parfaitement vous ramener

83
00:03:43,200 --> 00:03:44,954
à des décisions binaires.

84
00:03:46,578 --> 00:03:48,944
Si vous faites if quelque chose,

85
00:03:49,044 --> 00:03:51,602
else if quelque chose,

86
00:03:51,702 --> 00:03:53,236
else if quelque chose, et cætera,

87
00:03:53,336 --> 00:03:54,692
derrière, vous avez un arbre binaire

88
00:03:55,139 --> 00:03:56,481
dans lequel vous avez deux choix.

89
00:03:56,581 --> 00:03:57,869
Alors évidemment, dans ce cas-là,

90
00:03:57,969 --> 00:03:59,602
vous avez un arbre qui est assez dégénéré

91
00:03:59,702 --> 00:04:00,879
mais vous avez un arbre quand même.

92
00:04:02,812 --> 00:04:05,871
Ça veut dire que vous pouvez avoir des arbres

93
00:04:06,895 --> 00:04:08,626
dans lesquels vous avez des choix multiples

94
00:04:08,726 --> 00:04:09,905
et vous pouvez vous ramener

95
00:04:10,005 --> 00:04:11,570
systématiquement à un arbre binaire,

96
00:04:12,048 --> 00:04:13,329
par contre, vous ne pouvez pas vous ramener

97
00:04:13,429 --> 00:04:14,483
directement à une liste

98
00:04:14,766 --> 00:04:17,113
ce qui serait un arbre avec un seul fil

99
00:04:17,702 --> 00:04:19,309
et ça, ce n'est pas suffisant

100
00:04:19,844 --> 00:04:22,362
pour représenter les décisions.

101
00:04:23,689 --> 00:04:27,324
Alors si on comptait un petit peu maintenant les arbres.

102
00:04:27,424 --> 00:04:29,335
Alors ici, on va faire juste des comptages

103
00:04:29,435 --> 00:04:31,537
de nœuds, de feuilles et de choses comme ça

104
00:04:31,637 --> 00:04:32,898
pour se rendre compte des volumes

105
00:04:32,998 --> 00:04:34,326
et de ce que ça représente.

106
00:04:34,945 --> 00:04:38,284
Si j'ai un arbre binaire de hauteur h,

107
00:04:40,229 --> 00:04:41,898
quel est le nombre minimum

108
00:04:41,998 --> 00:04:44,282
de nœuds que je peux avoir dans cet arbre ?

109
00:04:44,382 --> 00:04:45,748
Et quel est le nombre maximum de nœuds

110
00:04:45,848 --> 00:04:46,992
que je peux avoir dans cet arbre ?

111
00:04:47,461 --> 00:04:49,369
Si j'ai la hauteur h,

112
00:04:49,469 --> 00:04:51,414
au minimum je vais avoir

113
00:04:54,994 --> 00:04:59,993
h + 1 nœuds dans mon arbre de hauteur h.

114
00:05:01,132 --> 00:05:04,844
Si j'ai un arbre de hauteur h,

115
00:05:05,746 --> 00:05:07,871
vous voyez qu'à chaque étape

116
00:05:07,971 --> 00:05:09,156
je divise par 2,

117
00:05:10,124 --> 00:05:11,821
donc au maximum ici,

118
00:05:11,921 --> 00:05:14,276
je vais en avoir 8

119
00:05:14,376 --> 00:05:16,415
plus 4

120
00:05:16,515 --> 00:05:18,236
plus 2

121
00:05:18,336 --> 00:05:19,358
plus 1.

122
00:05:20,031 --> 00:05:23,392
Donc je vais en avoir 15.

123
00:05:23,492 --> 00:05:25,294
Donc celui-ci fait 15.

124
00:05:26,380 --> 00:05:28,172
Et 15 correspond exactement

125
00:05:28,272 --> 00:05:32,190
à 2 puissance le nombre de niveaux plus 1 moins 1,

126
00:05:32,718 --> 00:05:34,226
donc ça correspond exactement

127
00:05:34,326 --> 00:05:36,239
à cette forme-là.

128
00:05:36,772 --> 00:05:38,651
Ce qui est important à voir ici,

129
00:05:38,751 --> 00:05:39,857
c'est que si vous avez un arbre

130
00:05:40,686 --> 00:05:42,690
qui contient un maximum de nœuds,

131
00:05:43,125 --> 00:05:45,058
ici, vous voyez apparaître

132
00:05:45,158 --> 00:05:46,855
les puissances de 2

133
00:05:47,559 --> 00:05:50,006
qui sont les nombres de nœuds

134
00:05:50,106 --> 00:05:52,431
que vous avez à un niveau donné au maximum.

135
00:05:54,824 --> 00:05:57,116
Si maintenant je retourne la question,

136
00:05:57,216 --> 00:05:58,751
un arbre binaire ayant n nœuds,

137
00:06:01,494 --> 00:06:04,534
qu'est-ce qu'il a comme hauteur au minimum ?

138
00:06:05,044 --> 00:06:08,311
Il a en hauteur logarithme en base 2 de n

139
00:06:13,683 --> 00:06:14,945
et au maximum,

140
00:06:15,045 --> 00:06:17,835
il a n - 1 sommets.

141
00:06:19,620 --> 00:06:22,510
Le nombre de feuilles, là aussi, c'est quelque chose qui est important,

142
00:06:22,806 --> 00:06:24,776
si vous avez un arbre ayant n nœuds,

143
00:06:25,072 --> 00:06:27,752
vous avez le nombre de feuilles maximal,

144
00:06:27,852 --> 00:06:30,477
ça va être quand l'arbre est tassé,

145
00:06:30,577 --> 00:06:32,872
c'est-à-dire que tout est rabattu vers le haut,

146
00:06:33,333 --> 00:06:34,917
donc au minimum,

147
00:06:35,017 --> 00:06:37,758
vous avez une feuille,

148
00:06:37,858 --> 00:06:38,956
vous avez un fil,

149
00:06:39,056 --> 00:06:39,992
et au maximum,

150
00:06:40,234 --> 00:06:42,527
vous en avez de l'ordre de n/2.

151
00:06:43,067 --> 00:06:44,836
Et là, c'est important, ça veut dire

152
00:06:44,936 --> 00:06:46,515
qu'en gros,  quand vous avez un arbre

153
00:06:46,615 --> 00:06:47,589
qui est complet,

154
00:06:50,080 --> 00:06:52,081
vous avez en gros autant

155
00:06:52,564 --> 00:06:55,987
de feuilles ici

156
00:06:56,697 --> 00:06:59,955
que de nœuds ici.

157
00:07:01,954 --> 00:07:04,517
C'est ça qui est important à retenir.

158
00:07:05,100 --> 00:07:06,996
Ici, vous en avez 7.

159
00:07:07,740 --> 00:07:09,446
Et ici, vous en avez 8.

160
00:07:10,187 --> 00:07:12,072
C'est ça qui est important à retenir

161
00:07:12,390 --> 00:07:13,743
c'est-à-dire qu'en gros,

162
00:07:14,101 --> 00:07:16,161
si vous avez n objets dans un arbre

163
00:07:16,261 --> 00:07:17,651
qui est bien fichu,

164
00:07:18,403 --> 00:07:21,011
et que vous les avez mis sur des feuilles par exemple,

165
00:07:21,211 --> 00:07:23,565
et bien vous avez en tout

166
00:07:23,665 --> 00:07:26,078
besoin de n - 1 nœuds intermédiaires

167
00:07:26,453 --> 00:07:28,148
pour effectivement le mettre dans un arbre.

168
00:07:29,584 --> 00:07:31,500
Donc ça veut dire qu'une structure

169
00:07:31,600 --> 00:07:33,220
qui contient par exemple n feuilles

170
00:07:33,586 --> 00:07:37,013
n'aura pas de surcoût trop important,

171
00:07:37,113 --> 00:07:38,118
c'est un surcoût linéaire,

172
00:07:38,423 --> 00:07:40,899
par rapport à la place que l'on utilisera

173
00:07:40,999 --> 00:07:41,872
pour stocker l'arbre.

174
00:07:44,427 --> 00:07:47,038
On a plusieurs cas particuliers à connaître,

175
00:07:47,138 --> 00:07:48,460
les arbres dégénérés.

176
00:07:48,950 --> 00:07:51,292
Les arbres dégénérés, ce sont les fils.

177
00:07:51,392 --> 00:07:53,333
Attention, le fil n'est pas toujours avec

178
00:07:53,433 --> 00:07:55,581
les fils gauches qui descendent,

179
00:07:55,681 --> 00:07:58,453
ça peut être fils gauche, fils droit, fils droit, fils gauche, et cætera.

180
00:07:59,247 --> 00:08:02,362
Et puis des adaptations du fil

181
00:08:02,462 --> 00:08:04,150
qui sont ce qu'on appelle les peignes,

182
00:08:04,824 --> 00:08:07,102
c'est ce que je vous disais avant avec les else-if,

183
00:08:07,692 --> 00:08:09,193
et dans ce cas-là, vous avez

184
00:08:10,625 --> 00:08:12,791
quelque chose qui descend qui est le fil

185
00:08:12,891 --> 00:08:14,469
et puis à chaque fois vous avez un branchement,

186
00:08:15,188 --> 00:08:16,577
là, on aurait pu en mettre un aussi,

187
00:08:17,088 --> 00:08:19,819
des branchements qui vous permettent d'accéder à autre chose.

188
00:08:19,919 --> 00:08:21,216
Dans toutes ces structures en fait,

189
00:08:21,316 --> 00:08:22,340
ce qui est important,

190
00:08:22,440 --> 00:08:24,486
c'est qu'on a une structure linéaire qui est sous-jacente.

191
00:08:25,176 --> 00:08:26,448
Et puis d'un autre côté,

192
00:08:26,548 --> 00:08:28,717
on a l'arbre complet tel qu'on l'a vu avant,

193
00:08:29,289 --> 00:08:31,905
celui-là, on a tassé tous les nœuds vers le haut, au mieux,

194
00:08:32,005 --> 00:08:35,869
donc on a quelque chose qui discrimine très très bien.

195
00:08:36,275 --> 00:08:37,910
Au passage, cet arbre-là,

196
00:08:38,010 --> 00:08:39,279
on l'avait vu quand on fait

197
00:08:39,379 --> 00:08:40,793
par exemple la recherche dichotomique.

198
00:08:41,367 --> 00:08:44,157
Et puis, pour relâcher un peu cette hypothèse-là,

199
00:08:44,617 --> 00:08:45,677
on n'aura jamais

200
00:08:45,777 --> 00:08:47,211
un nombre d'éléments à mettre dans l'arbre

201
00:08:47,311 --> 00:08:49,252
qui sera 2 puissance h plus 1 moins 1

202
00:08:49,679 --> 00:08:52,153
donc on peut s'intéresser à des arbres

203
00:08:52,253 --> 00:08:55,274
dans lesquels vous avez un arbre qui est complet

204
00:08:57,025 --> 00:08:57,910
ici

205
00:08:58,373 --> 00:08:59,789
et vous avez des feuilles

206
00:08:59,889 --> 00:09:02,289
qui occupent le dernier niveau.

207
00:09:03,110 --> 00:09:05,147
Alors si on prend ce type

208
00:09:07,048 --> 00:09:08,866
d'arbre particulier, qu'est-ce qu'on a ?

209
00:09:09,298 --> 00:09:11,146
Pour le premier, on a une structure linéaire,

210
00:09:11,246 --> 00:09:12,772
c'est exactement ce que je vous ai dit avant.

211
00:09:13,121 --> 00:09:15,706
Et pour le deuxième, on a une hauteur

212
00:09:15,806 --> 00:09:16,730
qui est de l'ordre de log n

213
00:09:17,210 --> 00:09:19,944
et le nombre de feuilles

214
00:09:20,044 --> 00:09:20,931
est de l'ordre de n.

215
00:09:23,053 --> 00:09:26,249
Ce que l'on dit de façon un peu intuitive,

216
00:09:26,652 --> 00:09:28,220
le poids d'un vrai arbre,

217
00:09:28,320 --> 00:09:30,276
c'est-à-dire d'un arbre qui n'est pas dégénéré

218
00:09:30,376 --> 00:09:31,507
sous la forme d'un fil,

219
00:09:31,833 --> 00:09:32,884
est dans ses feuilles,

220
00:09:32,984 --> 00:09:35,483
c'est-à-dire dans les extrémités de ses branches.

