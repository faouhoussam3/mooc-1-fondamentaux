# 4.1.1 Script vidéos.
## Architecture matérielle. Introduction à l'architecture des systèmes informatiques

### Vidéo  1/2. Introduction à l'architecture des systèmes informatiques

**diapo 6** Objectif du chapitre

Ce premier chapitre, à travers une rapide histoire de l'architecture des ordinateurs, vise à sensibiliser l'auditeur comment les algortihmes ont sucité les développements matériels et comment les innovations matérielles ont permis et influencé certaines évolutions logicielles pour l'émergence de l'informatique puis du traitement massif des données et de l'intelligence artificielle.
Il présentera également les limites technologiques pour comprendre les freins au développement et les options de contournement choisies.

Cette histoire sera présentée en 2 parties, avant et après la limite du mur de chaleur.

**diapo 7** Histoire de l'info 1

Une première date est la publication par le mathématicien Muhammad Al Khwârizmi d'une classification des algorithmes existants au 9ème siècle. Le mot algorithme est issu de son nom.

 Au 17ème siècle, le philosophe et scientifique Blaise Pascal développe une machine d'arithmétique capable de faire mécaniquement des additions et soustractions et, par répétition, des multiplications et divisions. On la nomme aujourd'hui pascaline.

 Au 18ème siècle, l'inventeur français Joseph Jacquard automatise les métiers à tisser mécanique. Le motif est codé sous la forme d'une séquence d'instructions décrite par une carte perforée.

 En 1840, le mathématicien anglais Henri Babbage conçoit une machine à calculer mécanique, à vapeur, dont la séquence d'instruction est fournie par une carte perforée. La machine fonctionnant en décimal est complexe et jamais il ne terminera sa construction. Cependant, c'est pour cette machine, majoritairement théorique, que Ada Lovelace, comtesse anglaise, écrira les premiers programmes informatiques.

 Dans les mêmes temps, Georges Boole, mathématicien et philosophe britannique, formalise une algèbre binaire s'appuyant sur deux uniques états, 1 et 0. Elle est nommée depuis algèbre booléenne et, bien adaptée au relais, tubes et transistor, elle simplifiera beaucoup la réalisation de calculateurs.

 En 1936, Alan Turing, mathématicien britannique, propose un modèle théorique de machine permettant d'exécuter toute opération mathématique calculable à partir de quelques instructions simples, la machine de Turing universelle. Une machine est alors dite Turing-complète si elle permet de faire tout ce que le modèle théorique décrit.
Suivent alors plusieurs tentatives de réalisation, d'abord avec des relais mécaniques. Le premier ordinateur Turing-complet serait l'ordinateur électromécanique allemand binaire Zuse 3, détruit pendant la guerre.

 En 1945, sous la conduite du physicien américain John William Mauchly et de son compatriote ingénieur Presper Eckert, est développée une autre réalisation matérielle Turing-complète, cette fois utilisable durablement. L'ENIAC (Electronic Numerical Integrator And Computer) est financé par l'armée américaine pour résoudre des problèmes de balistique. Contrairement au Z3, il est décimal, ce qui augmente sa complexité, et utilise des tubes à vides, plus rapides que les relais.
L'ENIAC dispose de 17 000 tubes à vides, consomme 150 kW et est capable de 5000 additions, 357 multiplications ou 38 divisions par seconde. Les tubes étaient peu fiables, un insecte, parmi les premiers bugs informatiques, venant souvent mourir sur un tube brûlant pendant la journée, provoquant sa destruction.

**diapo 8** Histoire de l'info 2

 En 1947 commence l'histoire moderne de l'informatique. 3 physiciens américains des laboratoires Bell, William Shockley, John Bardeen, Walter Brattain découvrent l'effet transistor. Dans un semi-conducteur au germanium, un courant électrique permet de contrôler l'ouverture d'un circuit électrique. Il devient alors possible de remplacer les coûteux et peu fiables tubes à vide. Le transistor permet rapidement un bond en avant pour l'informatique, l'électronique, les télécommunications et est à l'origine de la création de l'électronique de puissance. Cette découverte est saluée par l'attribution du prix Nobel de physique en 1956.

Avec le transistor, complexe à fabriquer, apparaît la différenciation entre concepteur de processeur et développeur de programme.

 Dès 1958, Fairchild semiconducteur, fondée notamment par Gordon Moore pour développer des semiconducteurs à base de silicium, sort les premiers circuits intégrés regroupant de nombreux transistors gravés sur la même puce de silicium pour assurer des fonctions logiques.

 En 1968, Gordon Moore encore et 2 associés fondent la société Intel (pour Integrated Electronic) dans la naissante Silicon Valley et en 1971 commercialisent le premier processeur monolithique (sur une seule puce), l'Intel 4004, avec 2300 Transistors. La largeur de grille des transistors, ici 10 µm, caractérise la finesse de gravure des transistors. 

 Peu de temps après, Gordon Moore, toujours lui, corrige sa première prédiction et annonce un doublement du nombre de transistors par circuit intégré tous les 2 ans. A la fois anticipation et roadmap de l'industrie micro-électronique, cette loi continue d'être vérifiée 46 ans après, soit une multiplication par 2^23 = 8M du nombre de transistors sur une puce.

 1976 marque l'apparition de l'apple 1, rapidement remplacé par l'apple 2. Conçu pour être produits massivement à un prix raisonnable, ils contribuent à la diffusion de l'informatique. L'interface graphique facilitant son utilisation apparaîtra en 1986.

 En 1981, IBM en échec sur le marché des ordinateurs personnels, lance le PC (Personal Computer) basé sur une architecture ouverte qui sera reprise par de nombreux autres constructeurs.

Le développement des PC avec processeur Intel et dans une moindre mesure des ordinateurs Apple avec processeurs Motorola est tel qu'ils orientent l'évolution des technologies. Les processeurs généralistes progressent rapidement et les investissements pour suivre en terme de fabrication deviennent colossaux. Les autres architectures plus spécialisées comme les machines-langage, optimisés pour un langage, ne peuvent suivre et disparaissent.

 Le millénaire se termine avec la victoire aux échecs du calculateur Deep Blue contre le champion du monde et l'apparition du processeur Intel Pentium 3, comprenant 10 Millions de transistors de largeur de grille 200 nm. Il fonctionne avec une horloge à 1 GHz.

**diapo 9** Evolution des caractéristiques des processeurs

Le graphique montre l'évolution sans limite des caractéristiques des processeurs de 1970 à 2000. Nombre de transistor par puce, puissance de calcul par tâche, fréquence, puissance électrique augmentent conjointement.
 En 2002, on atteint une puissance de 30W/cm². Un processeur faisant environ 3 cm², le pentium IV en phase de calcul consomme 100W, 70 A sous 1,5 V. En comparaison, une plaque de cuisson du commerce annonce 7W/cm².
Cette première limite posée au développement de l'informatique est nommée mur de chaleur. L'augmentation de la fréquence des processeurs n'est plus tenable.

### Vidéo 2/2. Introduction à l'architecture des systèmes informatiques

**diapo 10** 

Le changement de millémnaire marque un tournant dans l'architecture informatique.
 Le mur de chaleur imposant une limite importante, plusieurs solutions vont amener l'architecture des processeurs à se complexifier pour tenir l'augmentation des performances requises par une informatique foisonnante.

**diapo 11** 

Les évolutions récentes de l'informatique doivent donc se faire malgré ce mur de chaleur, établi environ à 30W/m². La fréquence des processeurs restent par conséquent bloquée sous les 5 GHz.
 La plus visible des améliorations est l'augmentation du nombre de coeurs, rendue possible par la gestion multi-processing des systèmes d'exploitation.
 Par ailleurs, la performance par processus a continué d'augmenter, plus lentement, grâce à des améliorations matérielles (pipeline et instructions SIMD de gestion de tables de données) sur les processeurs. Ces améliorations, fortement liées au logiciel, seront au programme du chapitre 4 de ce module.
La technologie continue ainsi de suivre le loi de Moore, les principaux fabricants, Intel, AMD, IBM et NVidia ayant atteint 10 Mds de transistors par puce.
Notons par ailleurs que le coût des processeurs est resté globalement constant, conséquence d'une baisse du coût du transistor inversement proportionnelle à la loi de Moore.

**diapo 12** 

La chaleur n'ayant pas arrêté le développement des processeurs, la taille des atomes aura peut-être une influence... Sur cette Roadmap d'Intel, plus gros fournisseur de processeurs de PC, la limite physique ne semble pas encore atteinte. Au-dessous des 10nm, on compterait la largeur de grille en atomes de silicium. Le chiffre n'est donc plus la largeur de grille des transistors mais caractérise la densité de transistors. Ceux-ci étant conçu en 3 dimensions, leur densité augmente, avec une finesse de gravure semblable. La technologie de fabrication est devenue excessivement complexe et maitrisée par 3 acteurs au plus. Nous en reparlerons plus loin.

**diapo 13** Histoire de l'info 3


Reprenons notre frise historique un peu avant 2000 pour voir comment l'industrie informatique a contourné le problème du mur de chaleur.
En 1997, Nvidia sort le Riva 128, l'un des premiers processeurs graphiques, avec des instructions spécialisées permettant de gérer plus rapidement les calculs liés à l'affichage.
 La même année, Intel introduit dans le pentium MMX des instructions spécialisés dans le traitement de données en table (SIMD pour Single Instruction multiple Data), pour accélérer là encore le traitement multimédia.
 En 2005 AMD commercialise le premier processeur grand public à 2 coeurs.
 En 2007, l'émergence des smartphones fait apparaîtres des puces hybrides contenant un coeur de processeur ARM, un modem 3G et un processeur graphique. On parle de SoC pour Système On Chip. L'américain Qualcomm s'impose sur ce marché, sous-traitant la fabrication en Asie.
 Apple emboîte le pas de Qualcomm et conçoit, sans les fabriquer, ses propres SoC pour ses IPhones, le premier est le A4 sorti en 2010. Fort de cette expérience, Apple s'attaquera plus tard à la conception des processeurs de ses PCs.
Ces 2 exemples montrent qu'en sous-traitant la fabrication, la conception de puces sépcialisées est redevenue possibles pour un nombre plus importants d'acteurs. 
 En 2020, l'AMD Epyc contient 64 coeurs et 40 Milliards de transistors gravés en 7 nm.
 Un peu plus tôt, en 2017, AlphaGo, programme d'intelligence artificielle développé par DeepMind, la filiale IA de Google, bat tous les meilleurs joueurs de GO, jeu de stratégie pourtant réputé inaccessible aux machines.
 Ces algorithmes d'apprentissage sont performants parce qu'ils peuvent jouer des milliards de parties en temps accéléré sur de puissants serveurs. Les centres de recherche comme les entreprises s'équipent de calculateurs associants dans des réseaux très rapides de nombreux noeuds possédant des processeurs multicoeur, beaucoup de mémoire et des processeurs graphiques. On présente ici les caractéristiques du calculateur Jean Zay du CNRS au plateau de Saclay : 4348 processeurs Intel Cascade Lake, 2720 GPU Nvidia et 450 To de mémoire.
 En parallèle de ces développements pour des machines plus puissantes, une branche de l'informatique répond aux besoins des applications du quotidien (du four programmable à la voiture) et de l'internet des objets avec des System On Chip moins puissants, bon marché, petits et économes en énergie. Nous en reparlerons dans le chapitre sur les systèmes embarqués.

**diapo 14** Les principaux acteurs en 2021 des évolutions de l’informatique

Les progrès récents de l'informatique ont permis la diffusion massive d'une informatique performante et, avec le développement des réseaux, la constitution et le traitement de gigantesques bases de données par l'intelligence artificielle.
5 des 7 plus grandes entreprises mondiales en terme de capitalisation boursière ont fondé tout ou partie de leur évolution sur ces données. Apple, Google et Facebook tirent des revenus importants de la connaissance de leurs milliards d'utilisateurs. Microsoft Azure, Amazon Web Service et Google Cloud représentent 63% du marché du cloud computing ou info-nuagique en français.
Ces services s'appuient sur une informatique performante et participent à financer son développement.

Regardons qui conçoit les processeurs qui permettent les réussites de ces entreprises 
Intel, plus gros fournisseur de processeurs de PC et de serveurs, n'est que 57ème alors que NVidia, premier fournisseur des GPU recherchés pour les consoles, les PCs et les calculateurs, est 8ème en terme de capitalisation. Samsung est le premier fabricant de mémoires, AMD est le 2nd fabricant de processeurs de PC et Qualcomm est le premier fabricant de processeurs pour smartphone. Broadcom fournit des processeurs pour les télécoms et pour le calcul, notamment pour les voitures Tesla.

Observons alors qui fabrique les processeurs performants des PCs, serveurs et calculateurs.
 Seuls Intel, Samsung et TSMC ont le savoir-faire pour produire des processeurs ou mémoire de finesse inférieure à 10 nm.
En juillet 2021, seuls Samsung et TSMC fabriquaient en 7 nm, avec 92% de part de marché pour TSMC. 

Les concepteurs fabless Nvidia, AMD, Qualcomm, Broadcom... et même Intel font donc fabriquer leurs processeurs chez TSMC, Intel rattrapant actuellement son retard en fabrication.

Cette concentration des compétences de fabrication chez un seul fabricant, Taiwan Semiconductor Manufacturing Company, crée des problèmes certains d'approvisionnement, comme c'est le cas au 2nd semestre 2021 pour les constructeurs automobiles notamment. En contrepartie, ce savoir-faire d'excellence et ces usines à 20 Mds d'euros sont au service de toute entreprise souhaitant concevoir des processeurs. Ceci est facilité par la vente par ARM, société britannique rachetée par Nvidia, de design de coeurs de processeur extrêmement performants.  C'est ainsi que Apple conçoit depuis plusieurs années ses processeurs de smartphone et maintenant de PC, toujours avec des coeurs ARM, au dépends d'Intel,  Amazon conçoit depuis 2018 les processeurs Graviton de certains de ses serveurs, Google conçoit les processeurs Tensor des smartphones Pixel et les fait fabriquer par Samsung.
 Microsoft a conçu ses processeurs de tablette SQ1 en collaboration avec Qualcomm, là encore avec des coeurs ARM et une production TSMC,  Tesla prévoit de produire des processeurs TSD de conduite autonome dans les usines Samsung et  Facebook travaille à un processeur optimisé pour l'intelligence artificielle. IBM propose aussi des processeurs Power et Telum pour serveurs fabriqués en 7 nm par Samsung et annonce travailler au développement de technologies de fabrication 5 et 2 nm.

Les survols de l'espace aérien taiwanais par les avions chinois rappelle les tensions géopolitiques autour des usines TSMC. TSMC, Intel et Samsung prévoit de construire de nouvelles usines à 20 Mds d'euros, en Europe, en Asie et aux Etats-Unis, mais cela demande du temps et des machines.
C'est ici qu'intervient le dernier acteur de la chaîne :  ASML, fabricant néerlandais de machines de lithographie à 7 et 5 nm. Seul fabricant à dessiner aussi finement, avec de l'ultraviolet extrême (EUV) à 13,5 nm, il fournit Samsung, Intel et TSMC mais ses capacités de production sont un élément limitant le déploiement des technologies 7 et 5 nm.

Par ailleurs, sous la pression des Etats-Unis notammment, les fabricants chinois se voient bloqué l'accès à ces technologies de pointe, chez TSMC et chez ASML, ce qui limite le développement de puces chinoises performantes.

Notons enfin que excepté Amazon et Alibaba qui ont aussi une activité logistique, ces entreprises sont de taille modeste en nombre d'employés. Microsoft est 111ème entreprise mondiale selon ce critère, ASML 722ème !

**diapo 15**

A travers ce premier chapitre historique, nous pouvons retenir que :
 Le développement de l’informatique moderne commence en 1947 par l’invention du transistor,
 En 2000, le mur de chaleur bride de développement de processeurs toujours plus gros et toujours plus rapide,
 Depuis 2000, les processeurs deviennent plus complexes en terme d’architecture, des processeurs spécialisés apparaissent,
 Les architectures multicoeur aux instructions complexes, la cohabitation avec les GPU, rendent le lien avec le logiciel très étroit,
 La fabrication devenue extrêmement complexe est très concentrée des processeurs performants est un enjeu géopolitique.
 La prochaine disruption dans l'informatique sera peut-être l'apparition des ordinateurs quantiques. Je vous laisse un lien vers quelques conférences sur le sujet : https://www.universite-paris-saclay.fr/recherche/thematiques-et-structures/axes-et-grands-projets/quantum-centre-en-sciences-et-technologies-quantiques/lordinateur-quantique.
