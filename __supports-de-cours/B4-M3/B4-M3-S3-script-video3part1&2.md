# 4.3  Réseaux  -  IPv6

**diapo 2** Intro du chapitre
Les deux séquences précédentes ont présenté la couche réseau IPv4, permettant l'identification des machines et l'acheminement des paquets. Les grands principes d'IPv4 sont ceux d'IPv6, nombre d'équipements locaux ayant une durée de vie importante sont en IPv4 et certaines institutions trainent à adopter IPv6 en local. IPv4 garde donc son intérêt, d'autant plus qu'il est plus facile à manipuler en travaux pratiques.
Cependant, peu à peu, IPv4 est remplacé par le protocole IPv6, objet de cette séquence.
 

## 1. 
**diapo 3** Pourquoi IPv6
Les adresses IP publiques sont uniques sur le réseau Internet. Codées sur 32 bits, il en existe environ 4 Milliards. Elles sont attribuées par un organisme international, l'IANA (Internet Assigned Numbers Authority) qui délègue une partie des affectations à des Registres Intermédiaires Régionaux RIR.
La diffusion massive d'Internet chez les particuliers et la connexion des smartphones à ce réseau a augmenté la demande.
Comme le montre la courbe ci-dessous, depuis 2011, l'IANA n'a plus de blocs /8 d'adresses IPv4 disponibles et il en sera de même très bientôt pour l'ensemble des registres régionaux. On rappelle qu'un bloc /8 en notation CIDR correspond à 2 exposant 24 adresses, soit 16 Millions d'adresses environ.
Il est important de noter de plus que ces adresses sont affectées géographiquement de manière très inégale. L'Afrique, dernier continent à avoir démocratisé Internet, n'a par exemple que 1,6 % des blocs d'adresses qui lui sont attribués.

. La box du fournisseur d'accès à Internet, qui a une IP publique, leur envoie donc une adresse IP privée permettant, grâce au protocole NAT, d'accéder à Internet mais pas d'y être vu. Le routeur cache alors, derrière son adresse, tous les échanges avec les PCs de son réseau local. Ce peut-être un petit réseau local du domicile mais, avec la pénurie, ce protocole NAT s'est répandu à toute nouvelle institution ne pouvant obtenir qu'une adresse IP publique.
 En téléphonie mobile aussi, sauf demande particulière, le smartphone obtient une IP privée sur le réseau de son opérateur.

Le protocole, nommé NAT comme Network Adress Translation, sera présenté dans le chapitre suivant sur le routage.
Notons tout de même que NAT, en plus de complexifier la visibilité des machines sur Internet, brise la notion de protocole IP de bout en bout, l'adresse du paquet changeant en cours de route et que ce changement demande un peu de travail au routeur et donc introduit de la latence.

Enfin, la pénurie d'adresses IPv4 a obligé les registres régionaux à allouer des petits blocs à des utilisateurs éloignés géographiquement. Cet émiettement de l'espace IP complexifie et donc ralentit l'acheminement des paquets. Sur des nombres plus petits, on peut comparer cela au tri du courrier rendu plus long si le code postal 94110 était dans le nord de la France et le 94230 dans le sud.

L'introduction d'IPv6 au début des années 2000 vise à résoudre la pénurie d'adresses IP et à simplifier le protocole IP.

**diapo 3** En-tête IPv6
Le protocole IPv6 est très proche d'IPv4, plus simple même, sous certains points de vue.
La différence majeure tient dans la taille des adresses, défaut majeur de IPv4.
Les adresses passent de 32 bits à 128 bits et le nombre des adresses possibles de 4 Milliards environ à 3.10^38, soit plusieurs milliards par mm² de surface terrestre.
L'en-tête IPv4 avait une taille de 20 octets minimum. Celui de IPv6 fait toujours 40 octets. il peut être suivi par des en-têtes optionnels, notamment de gestion de la sécurité, au début du champ de données. 

Outre les adresses IP source et destination, on y trouve comme sur IPv4 : 
* le numéro de version,
* le numéro du protocole occupant le champ data du paquet IP (Next Header),
* le champ IPv4 type of Service est remplacé par un champ Traffic Class destiné lui aussi à gérer la qualité de service. 
* Le champ Flow Label, en désignant les paquets d'un même flux, permet de leur attribuer des ressources liées à la qualité de service.
* On retrouve le nombre de saut maximum à travers les routeurs. Ce champ change de nom : de TimeToLive à Hop Limit, les durées de vie des paquets étant mesurées en saut et non pas en secondes.
* La longueur du paquet devient longueur du champ de données (payload length), l'en-tête étant de taille fixe,
* Le CRC de l'en-tête disparait. En effet, les données transportées par les paquets IP ont leur propre Champ de Contrôle et les trames véhiculant les paquets IP (trames ethernet ou wifi) ont leur propre CRC également. La suppression de ce CRC qui devait être recalculé à chaque passage dans un routeur, le TTL changeant, permet d'accélérer les traitement des paquets et de diminuer la latence.

Notons qu'avec IPv6, la fragmentation des paquets IP n'est plus possible. La taille maximale d'un paquet IP, de sa source à sa destination, est nommée MTU (Maximum transmission unit). Un paquet trop gros pour une portion de réseau sera détruit et le routeur de cette portion enverra vers l'émetteur un message ICMP indiquant le MTU de la portion. Si le message ICMP est filtré, l'absence de réponse au paquet envoyé incitera l'émetteur à réémettre avec un MTU plus faible.
En IPv6, le MTU minimum est 1280 octets. Il est de 9 ko pour les jumbo frames et peut atteindre, en théorie, 4 Go pour les jumbograms, avec une extension de l'en-tête.




**diapo 4** Notation des adresses IPv6
L'adresse est noté en hexadécimal, par lot de 16 bits (donc 4 caractères), séparés de ":"
On utilise la notation CIDR, les premiers bits indiquant le réseau et les derniers désignant la machine. 
Le nombre de bits réseau est indiqué par un /, à la suite de l'adresse.
 Sur cet exemple /64, les 8 premiers octets sont le préfixe réseau et les 8 suivants identifient l'interface réseau de la machine. 
 Le préfixe réseau peut être séparé en préfixe global, de 48 à 64 bits, attribué par le fournisseur d'accès, et préfixe de sous-réseau, géré par l'établissement client de ce fournisseur d'accès.
 La notation autorise de ne pas écrire les 0 de poids forts des mots de l'adresse.
 La notation autorise également de remplacer une et une seule suite de 0 (qui peuvent être sur 2 à 16 octets) par le symbole ::.

 ::1/128 est l'adresse composée de 15 0 et terminée par un 1.
2000 :: /3 représente l'ensemble des adresses commençant par un 2 ou un 3.

**diapo 6** Quelques adresses IPv6 remarquables

 Les adresses __unicast globales__ sont les adresses IP routables sur Internet, permettant d'acheminer les paquets de leur source à leur destination. Ce sont les équivalents des IPv4 publiques. Elles commencent par les 3 bits 0, 0, 1. Ce sont donc toutes les adresses IPv6 commençant par un 2 ou un 3.
Ce sont les adresses IPv6 utilisées par les serveurs sur Internet, mais aussi par les clients, PC portable ou smartphone, le NAT n'existant pas en IPv6.
Le préfixe, sur 64 bit permet l'acheminement. La hiérarchie étant mieux respecté qu'en IPv4, un préfixe commun correspond à une localisation géographique commune. Les relais se contentent de connaître les préfixes.

 Les adresses __local link__, commençant par fe80 et 12 '0'), restent limitées au domaine de diffusion Ethernet et sont non-routables. L'interface choisit la partie identifiant de manière pseudo-aléatoire. Toute interface IPv6 a une adresse local link, dans le même sous-réseau fe80:"12'0'". Elle peut donc communiquer avec ses voisines, ce qui est exploité notamment par le protocole de découverte du voisinage ou par DHCPv6.
D'autres adresses locales, nommées adresses locales uniques LUA, et routables uniquement sur le réseau local sont également possibles. Nous n'en parlerons pas ici.

 Si on regarde les adresses d'une machine sur un réseau IPv6, elle a alors au moins deux adresses, une globale et une link locale. 
 Ici la machine a 2 deux adresses IPv6 globales, une permanente et une provisoire pour naviguer sur Internet. Les deux adresses on bien sûr le même préfixe, préfixe qui est aussi celui de la box.

 IPv6 ne propose pas, contrairement à IPv4, d'adresse de broadcast, type 192.168.1.255 sur un réseau 192.168.1.0/24. En revanche, l'ensemble d'adresses __multicast__ est beaucoup plus riche qu'en IPv4 et permet des fonctionnalités nouvelles, pour la diffusion multimédia. C'est l'objet de la diapositive suivante.

 Enfin, les adresses commençant par 8 bits à 0 sont réservées pour des __usages spéciaux__. Notons l'adresse ::/128 qui est utilisée pour une interface sans adresse, comme 0.0.0.0 en IPv4.
L'adresse loopback 127.0.0.1 utilisée en IPV4 par une interface pour s'adresser à elle-même est remplacée en IPv6 par ::1/128.


**diapo 7** Multicast
Les adresses multicast sont des adresses destination permettant à une trame envoyée par une source d'être acheminée vers plusieurs destinataires.
Les adresses commençant par ff0 sont les adresses dites well-known et servent pour la gestion du réseau. Par exemple, 
* ff02::1 est un multicast vers tous les noeuds dans la zone de diffusion, très utile pour la découverte de voisins
* ff05::2 est un multicast vers tous les routeurs du site et est utilisé pour les échanges liés aux algorithmes de routage

Les adresses Multicast de groupe vont faciliter la diffusion de contenu, multimédia, vers plusieurs adresses. Le protocole Multicast Listener Discovery MLD permet aux machines de s’abonner à un groupe multicast et de prévenir ainsi les routeurs qu’ils souhaitent recevoir les paquets de ce groupe.
Un émetteur du groupe envoie alors les messages au groupe et non à une machine.
Cela minimise l’usage de bande passante et est intéressant pour les applications de visioconférence ou pour le diffusion de la télévision via Internet.

 Lors d'une diffusion d'un contenu avec des adresses unicast, le serveur envoie un message à chaque destinataire transmettant l'information de bout en bout.

 En diffusion Multicast, les 2 PCs ont informé via des messages MLD les routeurs qu'ils sont intéressés par les messages du groupe, par exemple ff38::276. Le serveur émet alors un seul paquet à destination du groupe ff38::276. Celui-ci est transmis aux machines par les routeurs ayant des abonnés à ce groupe.

**diapo 7** NDP
En IPv4, le protocole ARP (Address Resolution Protocol) permet de retrouver l'adresse MAC d'une machine de la zone de diffusion grâce à son adresse IP. En IPv6, le protocole NDP (Neighbour Discovery Protocol) remplace ARP et y ajoute quelques fonctionnalités.
Pour la recherche d'un voisin, la machine source envoie une requête multicast dans la zone de diffusion. 
La requête multicast d'adresse ff02::1:{4 derniers octets de l'IP} se répand seulement dans la zone de diffusion. L'adresse ethernet elle-même est une adresse multicast commençant par 33:33 puis les 4 derniers octets de l'adresse IP recherchée.
En réponse, la machine visée renvoie un message NDP Neighbor Advertisement directement à l'adresse IP de la machine source du premier message.
Les 2 machines peuvent alors mettre à jour leur table NDP.

Notons que la fonction NDP comprend également une fonctionnalité recherche de routeur.

**diapo 8** IPv4 et IPv6
Une machine uniquement en IPv4 ne peut communiquer qu'avec une machine IPv4.
Une machine uniquement en IPv6 ne peut communiquer qu'avec une machine IPv6.

IPv6 est en cours d'adoption et IPv4 est encore présent pour longtemps, certaines machines industrielles fiables et onéreuses ayant des durées de vie de plusieurs décennies. 

La phase de cohabitations des adresses IPv4 risque donc de durer. Pour qu'elle se passe au mieux, afin de ne pas décourager les institutions adoptant IPv6, des mécanismes de cohabitation ont été prévu dès les débuts d'IPv6.


( __CLIC__) Le mieux est donc de doter le maximum d'équipements avec une double pile. Ils peuvent ainsi répondre en IPv6 à une sollicitation IPv6 et en IPv4 à une sollicitation IPv4. Beaucoup de serveurs sur internet répondent en IPv6 et IPv4.
On montre ici les résultats d'un échange de PING en IPv4 et IPv6 avec le serveur de Cisco.

Reste à étudier le cas de machines devant communiquer en IPv4 à travers un réseau IPv6 ( __CLIC__) et le cas moins courant de machines IPv6 qui devront traverser un réseau IPv4 ( __CLIC__). A chaque fois sur le chemin devront se trouver des équipements ayant la double pile IPv4 IPv6.

Des protocoles de tunnel où le paquet IPvx est encapsulé dans la plage de données d'un paquet IPvy existent et sont mis en oeuvre. Cependant, ils ralentissent Internet, notamment si les paquets IPvX doivent être fragementés pour rentrer dans le MTU des paquets IPvY.







Bibliographie :
IPv6 Summit 2015: IPv6 Multicast Technologies Tim Martin (Cisco) https://www.youtube.com/watch?v=H6bBiIPfYXM
Le cours IPv6 de fun mooc


 
