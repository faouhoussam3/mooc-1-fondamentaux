# Unité de quantité d'information

 Gilles Geeraerts. Comment mesurer l'information quand elle est exprimée en binaire.


[![Vidéo 2 B1-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M1-S2.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M1-S2.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M1/B1-M1-S2.srt" target="_blank">Sous-titre de la vidéo</a> -  <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M1/B1-M1-S2-script.md" target="_blank">Transcription de la vidéo </a>

## Supports de présentation (diapos)

<a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M1/Slides/B1-M1-S2.pdf" target="_blank">Supports de présentation</a>

## Texte complémentaire 

<a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/Le_Mooc/bloc_1_repr_donnees_information/1-1_types_valeurs_de_base/1-1-2_Unite_de_quantite_d'information/1-1-2-1_texte.html" target="_blank">Texte complément à la video</a>