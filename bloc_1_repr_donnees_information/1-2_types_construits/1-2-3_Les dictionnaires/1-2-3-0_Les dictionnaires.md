## Les dictionnaires

- le tableau,
- le p-uplet,
- **le dictionnaire**

####

- Sébastien Hoarau. Les dictionnaires

[![Vidéo 1 B1-M2-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M2-S3.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M2-S3.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M2/B1-M2-S3.srt" target="_blank">Sous-titre de la vidéo</a> 

## Texte complémentaire 

<a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/bloc_1_repr_donnees_information/1-2_types_construits/1-2-3_Les%20dictionnaires/1-2-3-1_Fiche-dictionnaires.md" target="_blank">Texte complément à la video</a>


