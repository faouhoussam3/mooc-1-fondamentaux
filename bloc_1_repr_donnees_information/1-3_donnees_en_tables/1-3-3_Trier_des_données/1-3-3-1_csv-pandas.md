# CSV et Pandas

Impossible de parler de données en tables sans évoquer ces deux modules csv et pandas. Néanmoins, il n'est pas raisonnable d'envisager un cours complet sur ces modules dans le cadre de ce MOOC.

Nous ne faisons qu'une brève présentation et invitons le lecteur curieux à approfondir, soit par la documentation officielle (pour le module csv) soit par un des nombreux cours ou tutoriels existants pour le module pandas.


## Module csv 

Le module csv offre, entre autres, deux fonctions facilitant la lecture des données en tables.

### **Sans** descripteur


Le fichier [langages.csv](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M3/tables/langages.csv?inline=false) contient quelques langages de programmation ainsi que leurs années de naissance. Ce fichier ne contient pas de descripteur.

La fonction `reader` du module csv va nous permettre de créer un tableau où chaque enregistrement sera aussi un tableau :

```python
import csv

def lecture(fichier, delimiter=';', encoding='utf-8'):
    with open(fichier, 'r', encoding=encoding, newline='') as csvfile:
        return list(csv.reader(csvfile, delimiter=delimiter))
```

```python
>>> LANGAGES = lecture('langages.csv')
>>> LANGAGES[:3]
[['fortran', '1954'], ['lisp', '1959'], ['cobol', '1960']]
```

### **Avec** descripteur

La fonction `DictReader` effectue le même travail de lecture des données mais en tenant compte du fait que la première ligne correspond aux descripteurs. Les enregistrements sont alors stockés sous forme de dictionnaires :

```python
def lecture(fichier, delimiter=';', encoding='utf-8'):
    with open(fichier, 'r', encoding=encoding, newline='') as csvfile:
        return list(csv.DictReader(csvfile, delimiter=delimiter))
```

```python
>>> L_PAYS[0]
{'code': 'AD',
 'nom': 'Andorra',
 'capitale': 'Andorra la Vella',
 'aire': '468',
 'pop': '77006',
 'continent': 'EU'}
```

**Remarque** : le paramètre `newline` de la fonction `open` de Python permet de gérer correctement les différents retours à la ligne. 


### Écrire des données

Le module sert aussi à écrire dans un fichier les données stockées dans des dictionnaires. Pour cela, il faut mettre en oeuvre la fonction `DictWriter` pour créer un objet descripteur. Les méthodes `writeheader` et `writerow` permettent alors d'écrire les lignes du fichier. Voici l'exemple de la [documentation Python du module](https://docs.python.org/fr/3.6/library/csv.html) :

```python
with open('names.csv', 'w', newline='') as csvfile:
    fieldnames = ['first_name', 'last_name']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    writer.writerow({'first_name': 'Baked', 'last_name': 'Beans'})
    writer.writerow({'first_name': 'Lovely', 'last_name': 'Spam'})
    writer.writerow({'first_name': 'Wonderful', 'last_name': 'Spam'})
```

Le paramètre des descripteurs n'est pas optionnel car l'ordre doit être spécifié.

## Module pandas

Contrairement au module csv, le module pandas n'est pas installé par défaut avec une installation standard de Python. Pour une installation du module, le mieux est de se référer au site officiel [pandas.pydata.org](https://pandas.pydata.org/).

Mais en général un simple :

```bash
pip install pandas
```

fera l'affaire. Pour les environnement basés sur `conda` consulter la documentation.


Pandas est un outil complet de manipulation de données, non un simple lecteur de fichiers csv. Nous n'entrerons pas dans le détail mais présentons ici la philosophie du module concernant les données en tables.

L'objet qui modélise une donnée _pandas_ est le `DataFrame`. Voici comment, grâce à la fonction `read_csv` du module pandas, nous pouvons créer un DataFrame des données en tables :

```python
import pandas

def lecture(fichier, delimiter=';', encoding='utf-8'):
    with open(fichier, 'r', encoding=encoding, newline='') as csvfile:
        return pandas.read_csv(csvfile, delimiter=delimiter)
```

```python
>>> PAYS = lecture('countries.csv')
>>> type(PANDAS)
pandas.core.frame.DataFrame
```

```python
>>> PANDAS
```
**sortie**

|    | 	code	| nom	 | capitale	  | aire	| pop	| continent |
| -- | --------: | -----: | ---------: | -----:  | -----: | ---------: |
| 0	  | AD | Andorra	| Andorra la Vella	| 468.0	| 77006	| EU |
| 1	  | AE | United Arab Emirates	| Abu Dhabi	| 82880.0 | 9630959 | AS |
| ... |	... | ... |	... | ... | ... | ... |
| 250 |	ZM | Zambia	| Lusaka | 752614.0 | 17351822 | AF |
| 251 |	ZW | Zimbabwe | Harare | 390580.0 | 14439018 | AF |


À partir d'un DataFrame, on peut extraire des `Series` (les colonnes) :

```python
>>> PAYS.nom   # ou PAYS['nom']
```

**sortie**

|    |       |
| -- | -----: |
| 0 | Andorra |
| 1 | United Arab Emirates |
| ... | ... |
| 251 | Zimbabwe |

Name: nom, Length: 252, dtype: object

```python
>>> type(PAYS.nom)
pandas.core.series.Series
```

La méthode `loc` permet de récupérer des données parmi ce _tableau_ : la première valeur de la méthode correspond à un critère sur les lignes, la deuxième à un critère sur les colonnes. On peut utiliser la version `iloc` lorsqu'on fournit un indice pour la ligne :

**Les informations concernant le premier enregistrement**

```python
>>> andorra = PAYS.iloc[0]
>>> andorra.nom
'Andorra'
>>> andorra
```
**sortie**

|     |      |
| --- | ---: |
| code       |                AD
| nom        |           Andorra
| capitale   |  Andorra la Vella
| aire       |             468.0
| pop        |             77006
| continent  |                EU

Name: 0, dtype: object

**Recherche multiple**

Voici sur un exemple ce que donne une requête sur plusieurs critères. Nous recherchons les pays d'Europe de moins de 10000km<sup>2</sup> :

```python
>>> ppe = PAYS.loc[(PAYS.aire < 10000.) & (PAYS.continent == 'EU'),:]
>>> ppe
```

**sortie**


|    | 	code	| nom	 | capitale	  | aire	| pop	| continent |
| -- | --------: | -----: | ---------: | -----:  | -----: | ---------: |
| 0	  | AD | Andorra	| Andorra la Vella	| 468.0	| 77006	| EU |
| 15 | AX | Åland | Mariehamn	| 1580.0 | 26711 | EU |
| 56 | CY | Cyprus | Nicosia	| 9250.0 | 1189265 | EU |
| ... |	... | ... |	... | ... | ... | ... |
| 205 |	SM | San Marino	| San Marino | 61.2 | 33785 | EU |
| 237 |	VA | Vatican City | Vatican City | 0.4 | 921 | EU |

Une dernière requête : nous voulons obtenir le nom, la capitale et l'aire des pays extrêmes c'est-à-dire soit tout petitS (moins de 1 000km<sup>2</sup>) soit très grands (plus de 5 000 000) et triés par ordre croissant sur l'aire (qui doit aussi être non nulle) :

```python
>>> extremes = PAYS.loc[(PAYS.aire > 0.0) & (PAYS.aire < 1000.) | (PAYS.aire > 5000000.),['nom', 'capitale', 'aire']].sort_values(by='aire')
```

Nous ne pousserons pas plus loin la présentation de ce module d'une très grande richesse et invitons le lecteur à approfondir en suivant un cours spécifique.



