# Sauvegarde des données

- 1.3.1 Introduction sur les données en table
- 1.3.2 Recherches dans une table
- 1.3.3 Trier des données
- 1.3.4 Fusion de tables
- 1.3.5 **Sauvegarde des données**
- 1.3.6 Jointure

####

- Sébastien Hoarau.  Manipuler des données en table : sauvegarder nos données 

[![Vidéo 1 B1-M3-S5 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M3-S5.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M3-S5.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M3/B1-M3-S5.srt" target="_blank">Sous-titre de la vidéo</a> 
