# Programmation orientée objet .

## Objectifs du module
Il s'agit d'aborder ici de façon pragmatique la programmation orientée objet à partir des aspects orientés objet du langage de programmation Python, puis d'un mini-projet réalisé en objet avec le module turtle de Python.

## Prérequis
Savoir programmer en Python

Les bases de la programmation impérative Python vous sont acquises. La matière correspond au contenu du MOOC "Apprendre à coder avec Python".

## Sommaire
Nous regardons les aspects orientés objet de Python. Nous présentons le vocabulaire de la Programmation orientée objet, la syntaxe Python pour les bases et quelques concepts plus avancés (comment réaliser l'encapsulation par exemple).

    0. Présentation du module
    1. POO : une première approche
    2. Projet Escape Game "le château"
    3. Les concepts clés de la POO 
    

## Enseignant

### Sébastien Hoarau

Maître de Conférence en Informatique à l’Université de la Réunion et membre de l'IREM de la Réunion. Il enseigne l’initiation à la programmation impérative avec le langage Python3 à des 1ère années scientifiques. Initiation aux techno du web aussi : HTML, CSS, JavaScript
