## Calcul de la complexité d'un algorithme

1. Complexité d'algorithme : introduction et schéma algorithmique
2. **Calcul de la complexité d'un algorithme** (2 vidéos)
3. Complexité : ordres de grandeur et comparaison des algorithmes
4. Analyse de la complexité d'un problème. Conclusion
5. Preuve d'algorithme


[![Vidéo 2 part 1 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1c.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1c.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S2/B3-M1-S2-video2a.srt" target="_blank">Sous-titre de la vidéo</a>

[![Vidéo 2 part 2 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1d.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1d.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S2/B3-M1-S2-video2b.srt" target="_blank">Sous-titre de la vidéo</a>

## Supports de présentation (diapos)

[Support de la présentation des vidéos 2 à 7](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S2/B3-M1-S2-part1.pdf)
