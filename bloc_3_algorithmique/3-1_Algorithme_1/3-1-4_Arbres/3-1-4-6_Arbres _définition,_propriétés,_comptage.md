## Arbres. 6 & 7/10

  1. Introduction à la séquence
  2. Types abstraits en algorithmique
  3. Pourquoi, pour qui ? 
  4. La pile
  5. La file
  6. **Arbres - définition, propriétés, comptage part1 & part 2**
  7. Arbres binaires 
  8. Parcours d'arbre
  9. Feuilles étiquetées


## Arbres - définition, propriétés, comptage : partie 1

[![Vidéo 6 B3-M1-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S4-V6.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S4-V6.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S4/B3-M1-S4-video6.srt" target="_blank">Sous-titre de la vidéo</a>

## Arbres - définition, propriétés, comptage : partie 2

[![Vidéo 7 B3-M1-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S4-V7.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S4-V7.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S4/B3-M1-S4-video7.srt" target="_blank">Sous-titre de la vidéo</a>

## Supports de présentation (diapos)

[Support de la présentation des vidéos 6 à 10](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S4/B3-M1-S4-part2.pdf)


