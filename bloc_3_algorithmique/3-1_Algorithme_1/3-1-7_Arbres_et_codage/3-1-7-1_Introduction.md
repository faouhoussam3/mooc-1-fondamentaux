## Arbres et codage. 1/4 : Introduction

1. **Introduction**
2. Transmission de l'information
3. Codage
4. Un codage optimal

Algorithmes glouton (Huffman), complexité d'un code, théorie de l'information(intro)

- Code de longueur variable, arbre, Kraft
- Arbre optimal et entropie
- Algorithme de Huffman (File à priorité)

[![Vidéo 1 B3-M1-S7 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S7-V1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S7-V1.mp4)

