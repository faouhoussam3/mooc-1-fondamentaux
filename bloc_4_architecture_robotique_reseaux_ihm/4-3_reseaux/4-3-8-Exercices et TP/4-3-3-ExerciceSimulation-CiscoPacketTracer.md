# Introduction à Cisco Packet Tracer

L'objectif de ce premier TP de simulation est d'introduire le logiciel de simulation Cisco Packet Tracer à travers la simulation d'un réseau du domicile.
Cet outil de simulation est proposé gratuitement par la société Cisco.
A la différence de GNS3, c'est un logiciel propriétaire qui ne propose que des équipements Cisco et c'est un simulateur et non un émulateur, avec des fonctionnalités moins complètes.

Pourquoi alors avoir choisi celui-ci : 

* Il est plus simple à utiliser que GNS3.
* Il est multiplateforme (Windows, Mac, Linux), à la différence de certains outils pédagogiques.
* Il est très très utilisé dans le monde, notamment pour passer les certifications cisco, attestations reconnues partout et pour cela très recherchées. On trouve donc de très nombreux tutoriaux (sur Youtube en particulier), dans toutes les langues, pour faire fonctionner mêmes des architectures et fonctionnalités complexes.
* Conçu pour la formation des administrateurs réseaux, il utilise des simulations d'équipements industriels très représentatives des véritables équipements réseaux et de leur fonctionnalités.

## Installation de Cisco Packet Tracer

Cisco Packet Tracer est disponible gratuitement, sans condition, sur le site de diffusion du savoir grand public de Cisco : https://skillsforall.com
Une fois un compte créé, le catalogue propose un cours _Getting Started with Cisco Packet Tracer_ : https://skillsforall.com/course/getting-started-cisco-packet-tracer

Depuis ce cours, on trouve le lien de téléchargement de Cisco Packet Tracer : https://skillsforall.com/resources/lab-downloads

SkillsforAll fournit quelques guides de démarrage pour Cisco Packet Tracer.

Cisco, en plus de ce nouveau site SkillsforAll, propose également depuis de nombreuses années un site de formation pour les futurs professionnels du réseau : https://www.netacad.com/ L'installation se fait ensuite très simplement.

L'inscription à Netacad est un peu plus complexe (l'établissement doit être connu de NetAcad,  la mise en place d’un partenariat est expliqué sur le site.). Une fois l'enseignant enregistré, NetAcad donne accès à un très grand nombre de cours et de travaux pratiques (sur Cisco Packet Tracer) ; l'enseignant peut donner accès à ces ressources à ses élèves. C'est donc très intéressant de trouver une voie pour y accéder.

## Premiers pas avec Cisco Packet Tracer

Le suivi des courtes vidéos d'introduction de SkillsforAll > Getting Started with Cisco Packet Tracer, est une bonne manière d'aborder le logiciel.

Pour simplifier le débogage, on utilisera pour les équipements réseaux de ces TP uniquement des swicthes 2960 et des routeurs 2911, en plus du routeur domicile qui a l'avantage d'avoir une configuration graphique.

### Présentation du logiciel
La barre d’équipements fournit les équipements (switch, routeur, PC, serveur…, uniquement de marque Cisco) à ajouter sur le schéma réseau.

__La barre d’outils principale__
C’est la barre d’outil la plus haute, on y trouve les fonctionnalités standard.
![image-3.png](./image-3.png)


__La barre d’équipements__

![image-4.png](./image-4.png)

Parcourir les différentes catégories et ajouter sur le schéma réseau :

* Un routeur Home Routeur,
* Un serveur Server-PT,
* Un PC de bureau,
* Un PC portable Laptop,
* Une carte SBC (Small Board Computer),
* Des connexions { PC Ethernet - Routeur GigabitEthernet } et { Serveur Ethernet - Routeur Internet }, à l’aide de câbles droits.

__La barre secondaire__

![image-5.png](./image-5.png)

A l’aide des outils de dessin et de commentaires, améliorer le schéma réseau.

![image-20.png](./image-20.png)

### Changement d’une carte réseau

Sur certains équipements terminaux ou réseau, il est possible de changer l’interface réseau.

Sur le PC portable, on peut par exemple :

* éteindre le PC avec son interrupteur marche / arrêt, 
* retirer la carte réseau RJ45, 
* ajouter une carte réseau Wifi,
* allumer de nouveau le PC.

![image-6.png](./image-6.png)

Sur la carte SBC, on remplace le wifi par une connexion cuivre Gb. Pour cela, on éteind la carte, on ajoute un module Ethernet (NM-1CGE) par-dessus le module Wifi en bas à droite de la carte et on rallume la carte :

![image-7.png](./image-7.png)

On peut alors relier avec un câble droit la carte SBC à une connexion Gigabit Ethernet du routeur.

### Configuration des équipements

__Configuration du routeur__
Le routeur Home Router dispose d’une interface graphique similaire à celle d’un routeur de particulier (ongler GUI pour Graphical Unit Interface). Dans le menu Setup, pour ce premier TP, on fixe côté WAN (Internet) l'adresse IP de la box et une passerelle fictive, et côté LAN les paramètres du DHCP. 

![image-8.png](./image-8.png)

Dans le menu Wireless, on fixe un nom de réseau (SSID) et dans le sous-menu wireless security une clé WPA2.

![image-9.png](./image-9.png)

![image-10.png](./image-10.png)

__Configuration du PC portable__
On configure le PC portable pour permettre sa connexion au routeur via le réseau Wifi.

![image-11.png](./image-11.png)

Son adresse IP est obtenue automatiquement, en DHCP.

__Configuration du serveur__
Le serveur a une adresse IP statique.

![image-12.png](./image-12.png)

__Configuration du PC__
Le PC est configuré en DHCP.

![image-13.png](./image-13.png)

__Configuration de la carte SBC__, elle aussi en DHCP

![image-14.png](./image-14.png)

La carte SBC, nano-ordinateur, servira de serveur au domicile. Il est plus simple qu'elle soit en DHCP, pour centraliser les configurations réseaux et pour qu'elle obtienne de la box les configurations de passerelle par défaut et de serveur DNS. Par contre, comme elle sera serveur, il est important qu'elle ait toujours la même adresse et donc de lui réserver une adresse IP.

Pour cela, on regarde son adresse MAC dans l'onglet de configuration et on utilise le menu DHCP Reservation de la box : 

![image-21.png](./image-21.png)

Il faut redémarrer la carte SBC pour qu'elle demande de nouveau une adresse IP et ainsi qu'elle soit à jour.

Après avoir noté les adresses IP statiques sur le schéma, on obtient ainsi le réseau suivant : 

![image-22.png](./image-22.png)

### Tests de communications

L’icône « Add Simple PDU » permet d’envoyer des messages d’un équipement à un autre, à la manière d’un ping. Le bouton Delete permet de supprimer les anciens messages. Lorsqu'il y a un routeur, il est parfois de faire 2 envois pour que la traversée du routeur soit effective.
Tester la communication de tous les équipements locaux vers le routeur puis de tous les éléments locaux vers le serveur.
Utiliser le mode Simulation (bouton en bas à droite de la fenêtre Packet Tracer) et observer les échanges en mode pas à pas.
Les filtres, en bas de la fenêtre de simulation permettent de supprimer les messages inintéressants pour cet exercice, par exemple les message STP (Spanning Tree Protocol).

![image-23.png](./image-23.png)

### Serveur Web

Modifier la page web du serveur (Onglet services de la fenêtre de configuration puis HTTP > index.html > (edit) ) :

![image-24.png](./image-24.png)

Afficher cette page sur le PC local (onglet Desktop puis Web Browser) :

![image-25.png](./image-25.png)

Utiliser le mode Simulation et observer les échanges lors d'un rafraichissement de la page.

### Programmation python

Ajouter un programme serveur en python sur la carte SBC (Menu Programming de la fenêtre de configuration puis New et template TCP Server python, avec un nom cohérent). Des exemples serveur TCP et client TCP sont fournis.

![image-26.png](./image-26.png)

Créer de la même manière un programme client TCP Python sur le PC, indiquer l'adresse du serveur, personnaliser le message et tester.

![image-27.png](./image-27.png)

Il est possible d'utiliser le mode simulation pour observer les paquets échangés.

Le menu _Options > Préférences_ permet de personnaliser le comportement de Cisco Packet Tracer.

## Création d’un premier réseau, routage statique

On reprend le réseau domicile avec un PC, un routeur en DHCP réalisé juste avant.
On ajoute un fournisseur d’accès à Internet.

Tester l’envoi d’un PING, en mode simulation.

![image-16.png](./image-16.png)

Observer l'affichage d'une page Web sur le PC du domicile. En mode Simulation, visualiser sur le volet de droite les échanges.
On montre ici les échanges, sur un réseau un peu plus complet : 
![image-17.png](./image-17.png)

Il est aussi possible de double-cliquer sur les messages du volet Simulation pour en observer les détails. En regardant les 2 premiers messages (PC->Box et Box->RouterFree), on peut mettre en évidence le NAT avec le remplacement par le routeur de l'adresse IP privée du PC par son adresse IP publique : 

![image-18.png](./image-18.png)

![image-19.png](./image-19.png)
