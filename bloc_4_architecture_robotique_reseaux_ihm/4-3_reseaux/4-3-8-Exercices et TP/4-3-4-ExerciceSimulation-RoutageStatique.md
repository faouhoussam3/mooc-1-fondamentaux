# Simulation de routage statique sur Cisco Packet Tracer

En repartant du réseau du domicile, créer le réseau plus ambitieux suivant :

![image-30.png](./image-30.png)

## Configuration du réseau

### Configuration des interfaces

Les configurations des interfaces peuvent se faire de manière graphique, en double-cliquant sur le routeur :

![image-35.png](./image-35.png)

Pour la configuration en ligne de commande (CLI), la touche tabulation provoque 'auto-completion de l'instruction.
`>Enable` permet d’ouvrir la console.
`# show ?` Permet d’indiquer les différentes types d’informations que peut afficher l’équipement.

`# show running-config` rappelle la configuration en cours.
`# show ip interface` présente seulement les configurations des interfaces.

![image-37.png](./image-37.png)

`#config terminal` ou plus simplement `#conf ter` permet d’entrer en mode configuration.
Pour supprimer une configuration, il suffit d’écrire « no » devant la ligne ayant généré cette configuration.

L’interface gigabitEthernet 0/0 peut être également nommée g0/0
On note `interface g0/2` par exemple pour entrer dans le menu de configuration de l'interface g0/2.
Ensuite `ip address 192.168.4.1 255.255.255.0` par exemple permet d'attribuer une adresse en ligne de commande, comme indiqué sur la figure ci-dessous.

![image-38.png](./image-38.png)


### Configuration du routeur en serveur DHCP

Le routeur du domicile, pour plus de réalisme, est client DHCP vis-à-vis de son opérateur (en plus d'être serveur pour les machines de son réseau local LAN).

La configuration du serveur DHCP des routeurs 2911 se fait obligatoirement en ligne de commande. Les commandes ci-dessous, pour un réseau local, sont à adapter au réseau des box du FAI.

Routeur(config) #interface gigabitEthernet 0/0
Router(config-if)#ip address 192.168.1.1 255.255.255.0
Router(config-if)#no shutdown
Router(config-if)#exit
Router(config)#ip dhcp pool RESEAU_DOMICILE
Router(dhcp-config)#network 192.168.1.0 255.255.255.0
Router(dhcp-config)#default-router 192.168.1.1
Router(dhcp-config)#dns-server 208.67.220.220
Router(dhcp-config)#exit
Router(config)#exit
Router#write memory

Pour visualiser les configurations du serveur DHCP :
`show ip dhcp pool`
`show ip dhcp binding`

### Configuration des routes statiques

Configurer les routes, en utilisant une route par défaut pour chaque routeur. On donne ici un exemple utilisant l'interface graphique : 

![image-36.png](./image-36.png)

Pour ajouter une route en ligne de commande, il est possible, en mode config d'utiliser la ligne suivante :
`Router(config)# ip route @_réseau @_netmask @_passerelle`

Pour configurer la route par défaut (route vers tous les réseaux non nommés par les règles de routage), on procède de manière graphique ou en ligne de commande : 
`Router(config)# ip route 0.0.0.0 0.0.0.0 @_passerelle`

Pour voir la configuration des routes : `show ip route`

## Validation du bon fonctionnement

L'envoi de messages Simple PDU permet de vérifier les communications, noeud par noeud. L'affichage en mode simulation pas par pas de leur avancement permet de bien voir où il y a mauvais routage le cas échéant.

Vérifier que les PCs du domicile peuvent afficher les pages web du serveur Orange et du serveur ENS. Vérifier que le serveur ENS peut afficher la page web du serveur Orange.



