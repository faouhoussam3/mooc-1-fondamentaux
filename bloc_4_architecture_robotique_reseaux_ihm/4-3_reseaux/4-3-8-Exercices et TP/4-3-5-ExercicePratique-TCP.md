# Exercice pratique - Connexion TCP et ports

L'objectif de ce TP est d'observer l'établissement d'une connexion TCP et les ports source et client.

Pour cela, on utilise Wireshark et des requêtes vers des serveurs divers (web, mail, FTP, streaming...) pour observer les ports et le protocole de transport choisi.

Présenter les acquisitions wireshark de l'établissement d'une connexion TCP.
Qui est le client ? Qui est le serveur ?
Quel est le port client ? Quel est le port serveur ?
Qui prend l'initiative de la clôture de la connexion ?

Pour observer des échanges autres qu'un site web, repérer l'adresse IP du serveur à l'aide d'un ping puis filtrer avec la commande `ip.addr==x.y.z.w`
![image-34.png](./image-34.png)

