# Déploiement d'un site web

1. Introduction à HTML 5
2. **Déploiement d'un site web**
3. Interactions et gestion dynamique dans une page web
4. Frameworks de développement et outils de gestion de contenu

####

* **Rodrigue Chakode**. Comment héberger les pages web pour les rendre accessibles sur le web.
 
[![Vidéo 1 B4-M4-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M4-S2-video1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M4-S2-video1.mp4 )

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M4/NSI-B4-M4-S2-diapos.pdf" target="_blank">Supports de présentation de la vidéo </a>

## Exercice d'application 

<a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/Le_Mooc/bloc_4_architecture_robotique_reseaux_ihm/4-4_Technologies_web/4-4-5-TP/TP4_Deploiement_site_web.html"> TP 4 : Déploiement d'un site web (120 mn)</a>

